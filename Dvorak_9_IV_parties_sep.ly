\version "2.19.82"

\include "header.ly"

\include "parts/Violons.ly"
inst = "Violons I"
linst = "Violons I"
sinst = "Vlns.I"
notes = \ViolonI
outputName = "PDF/01-ViolonsI"
\include "piece.ly"

\include "parts/Violoncelle.ly"
inst = "Violoncelles"
lints = "Violoncelles"
sinst = "Vlcs."
notes = \Violoncelle
outputName = "PDF/02-Violoncelles"
\include "piece.ly"

\include "parts/Flute.ly"
inst = "Flûtes"
lints = "Flûtes tarversières"
sinst = "Flts."
notes = \Flute
outputName = "PDF/03-Flutes"
\include "piece.ly"


\include "parts/Hautbois.ly"
inst = "Hautbois"
lints = "Hautbois"
sinst = "Htbs."
notes = \Hautbois
outputName = "PDF/04-Hautbois"
\include "piece.ly"

\include "parts/Clarinette.ly"
inst = "Clarinettes"
linst = "Clarinettes en sib"
sinst = "Clar."
notes = \Clarinette
outputName = "PDF/05-Clarinettes"
\include "piece.ly"

\include "parts/SaxAlto.ly"
inst = "Saxophone Alto"
linst = "Saxophone Alto"
sinst = "SaxA."
notes = \SaxAlto
outputName = "PDF/06-Saxophone-Alto"
\include "piece.ly"

\include "parts/Cor.ly"
inst = "Cor en Fa"
linst = "Cor en Fa"
sinst = "Cor"
notes = \Cor
outputName = "PDF/07-Cor"
\include "piece.ly"

\include "parts/Trompette.ly"
inst = "Trompette"
lints = "Trompette en sib"
sints = "Trmp."
notes = \Trompette
outputName = "PDF/08-Trompette"
\include "piece.ly"

\include "parts/SaxTenor.ly"
inst = "Saxophone Ténor"
lints = "Saxophone Ténor"
sinst = "SaxT."
notes = \SaxTenor
outputName = "PDF/09-SaxTenor"
\include "piece.ly"

\include "parts/Trombone.ly"
inst = "Trombone"
linst = "Trombone"
sinst = "Trmb."
notes = \Trombone
outputName = "PDF/10-Trombone"
\include "piece.ly"

\include "parts/Timbale.ly"
inst = "Timbale"
linst = "Timbale"
sinst = "Timb."
notes = \Timbale
outputName = "PDF/11-Timbale"
\include "piece.ly"

\include "parts/Piano.ly"
inst = "Piano"
linst = "Piano"
sinst = "Piano"
notes = \Piano
outputName = "PDF/12-Piano"
\include "piece.ly"

\include "parts/Harpe.ly"
inst = "Harpe"
linst = "Harpe"
sinst = "Harpe"
notes = \Harpe
outputName = "PDF/13-Harpe"
\include "piece.ly"

\include "parts/Alto.ly"
inst = "Altos"
linst = "Altos"
sinst = "Altos"
notes = \Alto
outputName = "PDF/14-Altos"
\include "piece.ly"

\include "parts/Violon2.ly"
inst = "Violons II"
linst = "Violons II"
sinst = "Vlns.II"
notes = \ViolonII
outputName = "PDF/15-ViolonsII"
\include "piece.ly"

\include "parts/Tuba.ly"
inst = "Tuba"
linst = "Tuba Contrebasse"
sinst = "Tuba"
notes = \Tuba
outputName = "PDF/16-Tuba"
\include "piece.ly"
