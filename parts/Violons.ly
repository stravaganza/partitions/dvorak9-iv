\version "2.19.82"

\include "header.ly"

ViolonI =  \relative c' {
    \StandardConf
    \clef "treble" \key e \minor
    \tempo "Allegro con fuoco"4=152

    {%
        b4.\ff( c8) r2 |
        b4.( c8) r2 |
        b4( c8) r b4( c8) r |
        b'8 c16 r b8\< cis16 r b8 dis16 r b8 e16 r\! |
        b8\< fis'16 r b,8 g'16 r b,8 a'16 r b,8 ais'16 r |
        b4\ff( c8) r b4( c8) r |
        b4( cis8) r b4( dis8) r |
        r4 ees-.\f ees-. ees-. |
        dis1\prall\trill\<
    }%
    {% A
        \mark\default
        e4\ff r r2 |
        <c e,>4 r r2 |
        <g b, e,>4 r <b b, d,> r |
        <g b, e,> r r2 |
        <b e, g,>4 r r2 |
        <c e, fis,>4 r r2 |
        <b b,>4 r <dis dis,> r |
        e8:16 b: g: fis: e: b: e: g: |
        <b b,>4 r r b,16 e g b |
        <c e,>4 r r2 |
        <b e, g,>4 r <b d, g,> r |
        <g, e' b'> e'16 g b e b4 r |
        <b e, g,> r r b,16 e g b |
        <c e,>4 r r2 |
        <b b,>4 r <dis dis,> r |
        e8:16 b: g: fis: e: b: e: g: |
    }% End A
    {% B
        \mark\default
        b2-^\ff c4-. d-. |
        c4.-^ b8 b2 |
        b-> a4-> g8 a |
        \grace fis,8 <b' dis,>2. b4 |
        b2-^ c4-. d-. |
        c4.-^ b8 b2 |
        b a4-> g8 a |
        \grace fis,8 <b' dis,>2. b4 |
        <e e,>2-> <fis fis,>4-. <g g,> |
        <fis fis,>4. <e e,>8 <e e,>2 |
        <e e,> <d d,>4 <b b,>8 <d d,> |
        <e e,>2. <e e,>4 |
        <e e,>2 <fis fis,>4 <g g,> |
        <a a,>4. <g g,>16 <a a,> <b b,>2 |
        <g g,>16 <fis fis,> <e e,>4. <fis fis,>4 <dis dis,> |
        <g g,>16 <fis fis,> <e e,>4. <fis fis,>4 <dis dis,> |
        <g g,>16 <fis fis,> <e e,>8 <fis fis,> <dis dis,> <g g,>16 <fis fis,> <e e,>8 <fis fis,> <dis dis,> |
        <g g,>16 <fis fis,> <e e,>8 <fis fis,> <dis dis,> <fis fis,> <dis dis,> <fis, fis,> <dis dis,> | 
    }% End B
    {% C
        \mark\default
        \tuplet 3/2 {e8 fis g} \tuplet 3/2 {d b d} \tuplet 3/2 {e fis g} \tuplet 3/2 {d b d} |
        \tuplet 3/2 {e fis g} d'8. c16 b8. g16 fis8. e16 |
        \tuplet 3/2 {d8 fis e} \tuplet 3/2 {d fis e} \tuplet 3/2 {d fis a} \tuplet 3/2 {d a fis} |
        \tuplet 3/2 {e8 fis g} \tuplet 3/2 {d b d} \tuplet 3/2 {e fis g} \tuplet 3/2 {d b d} |
        \tuplet 3/2 {e fis g} d'8. c16 b8. g16 fis8. e16 |
        \tuplet 3/2 {e8 fis g} \tuplet 3/2 {fis b b,} \tuplet 3/2 {e fis g} e r |

        r4 b,8->\ff r r2 |
        r4 b8-> r r2 |
        r4 b8-> r r2 |
        r4 b8-> r r2 |

        \tuplet 3/2 {e'8 fis g} \tuplet 3/2 {d b d} \tuplet 3/2 {e fis g} \tuplet 3/2 {d b d} |
        \tuplet 3/2 {e fis g} d'8. c16 b8. g16 fis8. e16 |
        \tuplet 3/2 {d8 fis e} \tuplet 3/2 {d fis e} \tuplet 3/2 {d fis a} \tuplet 3/2 {d a fis} |
        \tuplet 3/2 {e8 fis g} \tuplet 3/2 {d b d} \tuplet 3/2 {e fis g} \tuplet 3/2 {d b d} |
        \tuplet 3/2 {e fis g} e'8. c16 b8. a16 fis8. e16 |
        \tuplet 3/2 {fis8 b d} fis8. e16 d8. c16 b8. fis16 |

        \tuplet 3/2 {g8 bes e} g8. e16 \tuplet 3/2 {e,8 g bes} e8. cis16 |
        \tuplet 3/2 {cis,8 e g} bes8. g16 \tuplet 3/2 {g,8 bes cis} e8. cis16 |
        \tuplet 3/2 {g8\mp\( bes cis} e8.-> cis16\) \tuplet 3/2 {g8\( bes cis} e8.-> cis16\) |
        \tuplet 3/2 {g8\p\( bes cis} e8.-> cis16\>\) \tuplet 3/2 {g8\( bes cis} e8.-> cis16\) |
        e8.-> cis16 e8.-> cis16 e8.-> cis16 e8.-> cis16 |
        e1~\! |
    }
    {% D
        \mark\default
        \tempo 4=120
        2\ppp r |
        R1 |
        c1:32\pp |
        d: |
        d:\< |
        d:\> |
        e2.:\pp d4: |
        1: |
        1: |
        1: |
        b:\pp |
        c4: a: bes2: |
        a1: |
        1: |
        1: |
        2: g4: bes: |
        a1: |
        2: a4. \< 8 |
        c2.\p b4 |
        b\<( g') fis( e) |
        2.\mp d4 |
        4\mf( b') a( g) |
        d'( b) a( g) |
        fis'\f( e\<) d( c) |
        b2\!( d) |
        b\<( a) |
    }
    {% E
        \mark\default
        \slashedGrace d,,8 <b' g'>4.\ff( b'8) a4.( g8) |
        a4.( c8) b4.( a8) |
        \acciaccatura ais b-. b-. b-. b-. b-. b-. \acciaccatura ais b-. b-. |
        <d,, d' b'> r <d c' a'> r \slashedGrace d <b' g'>2 |
        \slashedGrace d,8 <b' g'>4.\ff( b'8) a4.( g8) |
        a4.( c8) b4.( a8) |
        \acciaccatura ais b-. b-. b-. b-. b-. b-. \acciaccatura ais b-. b-. |
        <d,, d' b'> r <d c' a'> r  <d b' g'>4. r8 |
        <g' g'>4.( <fis fis'>8) <g g'>4.( <b, b'>8) |
        a'4->( c->) b->( a->) |
        <g g'>4.( <fis fis'>8) <g g'>4.( <b, b'>8) |
        a'4->( c->) b->( a->) |
        \slashedGrace ais8 b b \slashedGrace ais b b \slashedGrace ais b b \slashedGrace ais b b |
        <d,, d' b'> r <d c' a'> r \slashedGrace d <b' g'>2 |
        d8->\f g-. fis-> dis-. b16-> b'-. g-. fis-. e8 b |
        d8-> g-. fis-> dis-. b16-> b'-. g-. fis-. e8 b |
        d8-> g-. fis-> dis-. b16-> b'-. g-. fis-. e8 b |
        d8-> g-. fis-> dis-. b16-> b'-. g-. fis-. e8 b |
        R1*4 |
        d8->\mp g-. fis-> dis-. b16-> b'-. g-. fis-. e8 b |
        d8-> g-. fis-> dis-. b16-> b'-. g-. fis-. e8 b |
        d8->\p g-. fis-> dis-. b16-> b'-. g-. fis-. e8 b |
        d8-> g-. fis-> dis-.\> b16-> b'-. g-. fis-. e8 b |
        b16\pp g' e d b8 a g16 d' b a g8 e |
        d16( b') g e d b' g e d b' g e d b' g e |
        d8\pp( b') a g d' c b g' |
        e( d) c' a g b d e |
        <g, g'>1:32\ppp |
        1: |
        1: |
        1: |
        1: |
        1: |
    }
    {% F
        \mark\default
        8 r r4 g16\pp b g b fis a r8 |
        e16 g e g e g r8 d16 fis d fis c e r8 |
        b16 d b d b d r8 a16 c a c g b r8 |
        \repeat tremolo 8 {g16\< b} |
        \repeat tremolo 2 {ees,\f a} \repeat tremolo 6 {ees16\> a} |
        \repeat tremolo 2 {ees\mf a} \repeat tremolo 6 {ees\> a} |
        \repeat tremolo 2 {b\p fis} \repeat tremolo 6 {b\> fis} |
        \repeat tremolo 8 {b\! fis} |
        b16\p g b g d' b g' d g b g b fis a r8 |
        e16 g e g e g r8 d16 fis d fis c e r8 |
        b16 d b d b d r8 a16 c a c g b r8 |
        \repeat tremolo 8 {g16\< b} |
        \repeat tremolo 8 {c,\f a'} |
        \repeat tremolo 8 {c, a'} |
        \repeat tremolo 8 {a\> b} |
        \repeat tremolo 8 {a\p b} |
        e,8 r r4 r2 |
        \tuplet 3/2 {e'8\pp fis g} \tuplet 3/2 {d b d} \tuplet 3/2 {e fis g} d8 r |
        R1 |
        \tuplet 3/2 {e8 fis g} \tuplet 3/2 {d b d} \tuplet 3/2 {e fis g} d8 r |
        R1 |
        \tuplet 3/2 {e8 f g} \tuplet 3/2 {d b d} \tuplet 3/2 {e f g} d8 r |
        R1 |
        \tuplet 3/2 {e8\> f g} \tuplet 3/2 {d b d} \tuplet 3/2 {e f g} \tuplet 3/2 {d c d} |
        \tuplet 3/2 {e\f f g} \tuplet 3/2 {bes,\< c des} \tuplet 3/2 {g, a bes} \tuplet 3/2 {e, f g} |
        \tuplet 3/2 {bes,\! c des} r4 r2 |
        R1*2 |
    }
    {% G
        \mark\default
        R1 |
        r2 r4 \tuplet 3/2 {c''8 8 8} |
        f,4-. 4-. r2 |
        R1*4 |
        r2 r4 \tuplet 3/2 {cis'8 8 8} |
        fis,4-. 4-. r2 |
        R1*7 |
        r2 b,4:16\pp c: |
        d:\< e: fis: g: |
        g:\! a: b: c: |
        <d, d'>:\< <e e'>: <fis fis'>: <g g'>: |
        <bes bes'>1:32\ff |
        1: |
        r2 g,4:16\pp aes: |
        bes:\< c: d: ees: |
        f:\! fis: g: aes: |
        bes:\< c: d: ees: |
        <bes bes'>1:32\ff |
        1: |
        r4 ais,16\pp( b) 8-. ais'16( b) 8-. r4 |
        r4 ais,16( b) 8-. ais'16( b) 8-. r4 |
        r4 dis,16( e) 8-. dis'16\<( e) 8-. r4 |
        r4 dis,16\mp\<( e) 8-. dis'16( e) 8-. r4 |
        r4 dis,16\mf\<( e) 8-. dis'16( e) 8-. r4 |
        r4 dis,16\f\<( e) 8-. dis'16( e) 8-. r4 |
        R1\! |
        a,4->\f b8-.( c-.) b8.( a16) 4 |
        R1 |
        a4->\ff b8-.( c-.) b8.( a16) 4 |
        ais4->\f b8-.( cis-.) b8.( ais16) 4 |
        b4->\f cis8-.( d-.) cis8.( b16) 4 |
        d8 r16 cis16 4 d8 r16 cis16 4 |
        d8\< r16 cis16 4 d8 r16 cis16 4 |
    }
    {% H
        \mark\default
        <d, d'>2:16\ff 2:16 |
        2:16 2:16 |
        2:16 <bes' bes'>2:16 |
        <bes bes'>8:16 <a a'>: <g g'>: <f f'>: <ees ees'>: <d d'>: <c c'>: <bes bes'>: |
        <d d'>2:16\ff <bes' bes'>: |
        <bes bes'>8:16 <a a'>: <g g'>: <f f'>: <ees ees'>: <d d'>: <c c'>: <bes bes'>: |
        bes': a: g: f: ees: d: c: bes: |
        d': c: bes: a: g: f: ees: d: |
        <f f'>: <ees ees'>: <d d'>: <c c'> <bes bes'> <a a'> <g g'> <f f'> |
        <fis' fis'>:\< <e e'>: <d d'>: <cis cis'>: <b b'>: <ais ais'>: <g g'> <fis fis'> |
        e''1:32\fff |
        1: |
        4-. 4-. r4 4-. |
        r4 4-. r8 8-. 4-. |
        r4 4-. r4 4-. |
        r8 8-. 8-. r r4 4\fermata \bar "|."
    }
}


%temp
\new Score {
    \ViolonI
}