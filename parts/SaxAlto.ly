\version "2.19.82"

\include "header.ly"

SaxAlto = \relative c'' {
    \StandardConf
    \transposition ees
    \tempo "Allegro con fuoco" 4=152
    \clef treble \key cis \minor

    {%
        R1*7 |
        r4 bis-.\f bis-. bis-. |
        a8\< gis a gis a gis a' gis |
    }%
    {% A
        \mark\default
        cis,2->\ff dis4-. e-. |
        dis4.-> cis8 2 |
        cis b4 gis8 b |
        cis2.-> 4 |
        cis2-> dis4-. e-. |
        dis4.-> cis8 2 |
        cis4 \tuplet 3/2 {e8 cis e} gis4 gis, |
        cis2~8 r r4 |

        gis4 r r2 |
        a4 r r2 |
        gis4 r e r |
        e r r2 |
        gis4 r r2 |
        a4 r r2 |
        cis4 r bis r |
        cis2~8 r r4 |
    }
    {% B
        \mark\default
        R1*7 |
        r2 r4 gis4\ff |
        cis2-> dis4-. e-. |
        dis4.-> cis8 2 |
        cis b4 gis8 b |
        cis2.-> 4 |
        cis2-> dis4-. e-. |
        a,2-> e-> |
        fis2 4-. gis-. |
        fis2 4-. gis-. |
        fis4 8-. gis-. fis4 8-. gis-. |
        fis4 8-. gis-. fis-. gis-. fis-. gis-. |
    }
    {% C
        \mark\default
        e'1->\ff |
        cis4( dis) e fis |
        dis2( b8) r r4 |
        e1-> |
        cis4( dis) e fis |
        e8-. cis'-. gis-. gis,-. cis-. r r4 |
        dis4\p e dis cis |
        b a gis fis8 bis |
        dis4 e dis cis |
        b a gis fis |
        r8 e' r b r e r b |
        r e r fis r gis r ais |
        r fis r fis fis r r4 |
        r8 e r b r e r b |
        r e r4 r2 |
        r8 e r4 r2 |
        \tuplet 3/2 {e8 8 8} r4 \tuplet 3/2 {cis8 8 8} r4 |
        \tuplet 3/2 {ais8 8 8} r4 \tuplet 3/2 {e'8 8 8} r4 |
        R1*2 |
        e1\p\>~ |
        e\pp~ |
    }
    {% D
        \mark\default
        \tempo 4=120
        8 r r4 r2 |
        R1*19 |
        dis1\mp-> |
        e-> |
        gis-> |
        cis,2\mf\< d4 cis |
        e1\f |
        dis->\< |
    }
    {% E
        \mark\default
        e->\ff |
        dis-> |
        e-> |
        dis2-> e |
        1-> |
        dis-> |
        e-> |
        dis2-> e4. r8 |
        e4.( dis8) e4. e8 |
        dis4( fis) e( dis) |
        e4.( dis8) e4. e8 |
        dis4( fis) e( dis) |
        b8\f r bis r cis r cis r |
        b r b r b r r4 |
        b4\ff gis2->~ 8 r |
        r4 gis2->~ 8 r |
        r4 gis2->~ 8 r |
        r4 gis2->~ 8 r |
        R1*2 |
        b,1\p~ |
        1 |
        bis'\mp~ |
        1 |
        1\p~ |
        1~ |
        4\pp r r2 |
        R1*9 |
    }
    {% F
        \mark\default
        R1*4 |
        bis2->\f cisis4-. disis-. |
        cisis4. bis8 2 |
        aisis4-.\> gisis-. aisis-. bis-. |
        aisis2\p\> gisis\! |
        gis4 r r2 |
        R1*19 |
    }
    {% G
        \mark\default
        R1*16 |
        r2\p 4 r |
        4 r4 4\< r4 |
        4 r4 4 r4 |
        4 r4 4 r4 |
        e'1\ff~ |
        1 |
        bis4\pp r4 4 r4 |
        4 r4 4 r4 |
        4 r4 4 r4 |
        4 r4 4 r4 |
        e1\ff~ |
        1 |
        bis4\p 8 8 8 r16 16 8 8 |
        8 8 8 8 8 8 8 8 |
        e4 8-. 8-. 8-.\< r16 16-. 8-. 8-. |
        8-.\mp\< 8-. 8-. 8-. 8-. 8-. 8-. 8-. |
        fis4\fz 8-. 8-. 8-. r16 16-. 8-. 8-. |
        8-.\< 8-. 8-. 8-. 8-. 8-. 8-. 8-. |
        a,4.\ff c8-. dis-. fis4.->\fz |
        a2 r |
        a,4.\ff c8-. dis-. fis4.->\fz |
        a2 r |
        r cis,4\ff r |
        r2 dis4 r |
        g8\f 8 r4 8 8 r4 |
        8\< 8 r4 8 8 r4 |
    }
    {% H
        \mark\default
        e2->\ff fis4-. g-. |
        fis4.-> e8 2 |
        2-> d4 b8-. d-. |
        e2.-> r4 |
        2-> d4 b8-. d-. |
        e2.-> r4 |
        e1->\ff |
        d-> |
        d-> |
        fisis2-> cis-> |
        e1\fff~ |
        1 |
        4-. 4-. r4 4-. |
        r4 4-. r8 8-. 4-. |
        r4 4-. r4 4-. |
        r8 8-. 8-. r r4 4\fermata \bar "|."
    }
}

% Temp

\new Score{
    \SaxAlto
}