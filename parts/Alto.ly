\version "2.19.82"

\include "header.ly"

Alto = \relative c' {
    \StandardConf
    \clef alto \key e \minor
    \tempo "Allegro con fuoco" 4=152

    {%
        b4.\ff( c8) r2 |
        b4.( c8) r2 |
        b4( c8) r b4( c8) r |
        b16 b c8 b16\< b cis8 b16 b dis8 b16 b e8\! |
        b16\< b fis'8 b,16 b g'8 b,16 b a'8 b,16 b ais'8 |
        fis4\ff( g8) r fis4( a8) r |
        fis4( g8) r fis4( ais8) r  |
        r4 <bes g ees>-.\f <bes g ees>-. <bes g ees>-. |
        <a fis>8\< <a fis> <a fis> <a fis> <a fis> <a fis> <a fis> <a fis> | 
    }%
    {% A
        \mark\default
        <e, b' g'>4\f r r2 |
        <a fis' a>4 r r2 |
        <g e' b'>4 r <g d' b'> r |
        <g e' b'> r r2 |
        <e b' g'>4 r r2 |
        <a fis' a>4 r r2 |
        <g e' b'>4 r <b fis' b> r |
        <b g'>2:16 8 r r4 |

        <e, b' g'>4 r r b'16 e g b |
        <a, e' c'>4 r r2 |
        <g e' b'>4 r <g d' b'> r |
        <g e' b'>4 e16 g b e b4 r |
        <e b' g'>4 r r b16 e g b |
        <a, e' c'>4 r r2 |
        <g e' b'>4 r <b fis' b> r |
        <b g'>2:16 8 r r4 |
    }
    {% B
        \mark\default
        dis'2-^\ff e4-. fis-. |
        e4.-^ dis8 2 |
        b2-> fis4-> g8 e |
        b'2. dis4 |
        dis2-^ e4-. fis-. |
        e4.-^ dis8 2 |
        b2-> fis4-> g8 e |
        b'2. r4 |

        g,8:16\mf b: e: g: g: e: b: g: |
        a: c: e: fis: fis: e: c: a: |
        g: b: e: g: g,: b: e: g: |
        g,: b: e: g: g: e: b: g: |
        g: b: e: g: g: e: b: g: |
        e: a: c: e: e,: a: c: e: |
        c: e: c: e: c: e: fis,: b: |
        c: e: c: e: c: e: fis,: b: |
        e: c: c: b: e: c: c: b: |
        e: c: c: b: c: b: c: b: |
    }
    {% C
        \mark\default
        <g b> r8 8 r8 8 r8 8 r8 |
        <e g>4:16 <fis a>: <g b>: <a cis>: |
        <fis a>: <fis a>: <fis a>8 r r4 |
        <g b>8 r8 8 r8 8 r8 8 r8 |
        <e g>4:16 <fis a>: <g b>: <a cis>: |
        <b e>: <b dis>: <b e>8 r r4 |
        dis8:\mf fis: e: g: d: fis: c: e: |
        b: d: a: c: g: b: fis: b: |
        dis fis: e: g: d: fis: c: e: |
        b: d: a: c: g: b: fis: dis': |
        e r8 <g, b>8 r8 8 r8 8 r8 |
        <e g>4:16 <fis a>: <g b>: <a cis>: |
        <fis a>: <fis a>: <fis a>8 r r4 |
        <g b>8 r8 8 r8 8 r8 8 r8 |
        8 8 8 8 8 8 8 8 |
        <fis b>8 8 8 8 8 8 8 8 |
        <e bes'> r <bes' e>4 r <cis, g'> |
        r <cis e>-> r <cis e>-> |
        r g' r g |
        r g\p r g |
        R1*2 |
    }
    {
        \mark\default
        \tempo 4=120
        bes'1\ppp~ |
        1 |
        fis:32\pp |
        <fis a>: |
        g:\< |
        g:\> |
        <fis a>:\pp |
        1: |
        g: |
        1: |
        d:\pp |
        c2: <c e>: |
        c1: |
        1: |
        1: |
        2.: <c e>4: |
        c1: |
        1: |
        \repeat tremolo 8 {fis16 dis} |
        \repeat tremolo 8 {g16\< e} |
        \repeat tremolo 8 {a16\p fis} |
        \repeat tremolo 8 {b16\mf g} |
        \repeat tremolo 4 {d16\mf g} \repeat tremolo 4 {dis16 g} |
        \repeat tremolo 4 {e16 c} b16 gis b gis a b a b |
        \repeat tremolo 4 {d16 g} \repeat tremolo 4 {g16 b} |
        \repeat tremolo 8 {c,16\< d}
    }
    {% E
        \mark\default
        <b d>1:16\ff |
        <c ees>: |
        e4:-> dis:-> e:-> cis:-> |
        <c d>:-> 4: <b d>2: |
        1: |
        <c ees>: |
        e4:-> dis:-> e:-> cis:-> |
        <c d>2: <b d>2: |
        \tuplet 3/2 {b8( g') d'} \tuplet 3/2 {d( g,) b,} \tuplet 3/2 {b8( g') d'} \tuplet 3/2 {d( g,) b,} |
        \tuplet 3/2 {c8( g') ees'} \tuplet 3/2 {ees( g,) c,} \tuplet 3/2 {c8( g') ees'} \tuplet 3/2 {ees( g,) c,} |
        \tuplet 3/2 {b8( g') d'} \tuplet 3/2 {d( g,) b,} \tuplet 3/2 {b8( g') d'} \tuplet 3/2 {d( g,) b,} |
        \tuplet 3/2 {g8( g') ees'} \tuplet 3/2 {ees( g,) g,} \tuplet 3/2 {g8( g') ees'} \tuplet 3/2 {ees( g,) g,} |
        \tuplet 3/2 {g( e') b'} \tuplet 3/2 {b( dis,) g,} \tuplet 3/2 {g( e') b'} \tuplet 3/2 {b( e,) g,} |
        \tuplet 3/2 {g( fis') d'} \tuplet 3/2 {d( fis,) g,} \tuplet 3/2 {g( d') b'} \tuplet 3/2 {b( d,) g,} |
        g'-> d'-. b-> b,-. 16-> g-. e'-. b-. 8-. r |
        g'8-> d'-. b-> b,-. 16-> g-. e'-. b-. 8-. r |
        g'8-> d'-. b-> b,-. 16-> g-. e'-. b-. 8-. r |
        g'8-> d'-. b-> b,-. 16-> g-. e'-. b-. 8-. r |
        R1*2 |
        d,8-"pizz."\mf r d r d2 |
        8\> r d r d2 |
        b'1:16-"arco"\mp |
        1: |
        1:\p |
        1:\> |
        d\pp |
        d, ~ |
        1~ |
        8 r r4 r2 |
        r b''4-.-"pizz"\pp a-. |
        g-. r e-. d-. |
        b-. r r2 |
        R1*3 |
    }
    {% F
        \mark\default
        g'16\pp b g b g b g b g8 r8 a16 fis a fis |
        g e r8 g16 e g e fis d r8 e16 c e c |
        d b r8 d16 b d b c a r8 b16 g b g |
        \repeat tremolo 8 {g\< b} |
        \repeat tremolo 8 {fis\f\> ees} |
        \repeat tremolo 8 {fis\mf\> ees} |
        \repeat tremolo 8 {fis\p\> d} |
        \repeat tremolo 8 {fis\! d} |
        g\pp d g d g d b' g d'8 r a'16 fis a fis |
        g e r8 g16 e g e fis d r8 e16 c e c |
        d b r8 d16 b d b c a r8 b16 g b g |
        \repeat tremolo 8 {g\< b} |
        \repeat tremolo 8 {fis\f a} |
        \repeat tremolo 8 {fis a} |
        \repeat tremolo 8 {fis\> dis} |
        \repeat tremolo 8 {fis\p dis} |
        e1:16\sfp |
        R1 |
        e1:16\sfp |
        R1 |
        e1:16\sfp\> |
        R1\! |
        e1:16\sfp\> |
        R1\! |
        g8 r r4 r2 |
        \tuplet 3/2 {bes8 c des} \tuplet 3/2 {g, a bes} \tuplet 3/2 {e, f g} \tuplet 3/2 {c, d e} |
        f4 g8-. a-. g-. r16 f-. f8-. a-. |
        g-.\> f-. g-. a-. f-. a-. g-. f-.
    }
    {% G
        \mark\default
        f4\p g8-. a-. g-. r16 f-. 8-. a-. |
        g-. f-. g-. a-. f-. a-. g-. f-. |
        a-. g-. f-. a-. g-. f-. a-. g-. |
        f-. a-. g-. f-. a-. g-. f-. a-. |
        fis4\pp gis8-. ais8-. gis8-. r16 fis-. 8-. ais-. |
        gis-. fis-. gis-. ais-. fis-. ais-. gis-. fis-. |
        fis4 gis8-. ais8-. gis8-. r16 fis-. 8-. ais-. |
        gis-. fis-. gis-. ais-. fis-. ais-. gis-. fis-. |
        ais-. gis-. fis-. ais-. gis-. fis-. ais-. gis-. |
        fis-. ais-. gis-. fis-. ais-. gis-. fis-. ais-. |
        g4\fp a8-. b-. a-. r16 g-. 8-. b-. |
        a-. g-. a-. b-. g-. b-. a-. g-. |
        g4-> a8-. b-. a-. r16 g-. 8-. b-. |
        a-. g-. a-. b-. g-. b-. a-. g-. |
        b-. a-. g-. b-. a-. g-. b-. a-. |
        g-. b-. a-. g-. b-. a-. g-. b-. |
        a-. g-. b-. a-. g-. b-. a-. g-. |
        b-.\< a-. g-. b-. a-. g-. b-. a-. |
        g-.\! b-. a-. g-. b-. a-. g-. b-. |
        a-.\< g-. b-. a-. g-. b-. a-. g-. |
        <g ees'>1:32\ff |
        1: |
        g8-.\pp bes-. aes-. g-. bes-. aes-. g-. bes-. |
        aes-.\< g-. bes-. aes-. g-. bes-. aes-. g-. |
        bes-.\! aes-. g-. bes-. aes-. g-. bes-. aes-. |
        g-.\< bes-. aes-. g-. bes-. aes-. g-. b-. |
        <bes ees>1:32\ff |
        1: |
        fis2:16\pp g4: a: |
        g2: fis: |
        g2 a4:\< b: |
        a2: g:\< |
        a:\mf b4: c: |
        b2:\f a:\< |
        <c ees>1:\ff |
        <c ees>1: |
        <c ees>1: |
        <c ees>1: |
        e2: <cis e>: |
        <b d>:16 <d fis>: |
        <bes g'>2:16 2: |
        2:16\< 2: |
    }
    {% G
        \mark\default
        <bes d>:16\ff 2: |
        2:16 2: |
        2: <d f>: |
        <bes d>1: |
        2: <d f>: |
        <bes d>1: |
        2:16 2: |
        <f bes>:16 2: |
        2:16 2: |
        <e ais>:\< <e cis'>: |
        <g e'>1:32\fff |
        1: |
        4-. 4-. r <g d'> |
        r <g e'>-. r8 8-. 4-. |
        r4 4-. r <g d'> |
        r8 <g e'>8-. 8-. r r4 4 \fermata \bar "|."
    }
}

% Temp

\new Score{
    \Alto
}