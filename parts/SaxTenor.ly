\version "2.19.82"

\include "header.ly"

SaxTenor = \relative c''' {
    \StandardConf
    \transposition bes,
    \tempo "Allegro con fuoco" 4=152
    \clef treble \key fis \minor

    {%
        R1*7 |
        r4 a-.\f a-. a-. |
        gis1\< |
    }%
    {% A
        \mark\default
        fis4\f r r2 |
        fis4 r r2 |
        fis4 r e r|
        fis r r2 |
        fis4 r r2 |
        fis4 r r2 |
        fis4 r f r |
        fis2~8 r r4 |

        cis r r2 |
        fis4 r r2 |
        fis4 r e r |
        fis r r2 |
        fis4 r r2 |
        fis4 r r2 |
        fis4 r f r |
        fis2~8 r r4 |
    }
    {% B
        \mark\default
        R1*7 |
        r2 r4 cis\ff |
        fis2-> gis4-. a-. |
        gis4.-> fis8 2 |
        fis2 e4 cis8 e |
        fis2.-> 4 |
        fis2-> gis4-. a-.|
        fis2 2 |
        d2 4-. cis-. |
        d2 4-. cis-. |
        d4 8-. cis-. d4 8-. cis-. |
        d4 8-. cis-. d-. cis-. d-. cis-. |
    }
    {% B
        \mark\default
        a8\f r a r a r a r |
        a r b r cis r a r |
        gis r e' r gis, r r4 |
        a8 r a r a r a r |
        a r b r cis r a r |
        gis r gis r a r r4 |
        eis' fis e d |
        cis b a gis8 cis |
        eis4 fis e d |
        cis b a gis |
        fis'8 r a, r a r a r |
        a r b r cis r a r |
        b r b r b r r4 |
        a8 r cis r fis r cis r |
        a4 r r2 |
        gis4 r r2 |
        \tuplet 3/2 {bis8 8 8} r4 \tuplet 3/2 {a8 8 8} r4 |
        \tuplet 3/2 {fis8 8 8} r4 \tuplet 3/2 {b8 8 8} r4 |
        r4 \tuplet 3/2 {bis8\mf b bis} r4 \tuplet 3/2 {bis8 b bis} |
        r4 \tuplet 3/2 {dis,8\p 8 8\>} r4 \tuplet 3/2 {dis8 8 8\!} |
        R1*2 |
    }
    {%  D
        \mark\default
        \tempo 4=120
        R1*18 |
        gis'1\p |
        fis2.\< a4 |
        b1\! |
        a |
        a |
        2 ais4 b |
        a1 |
        gis1\< |
    }
    {% E
        \mark\default
        a\ff |
        gis |
        a |
        gis2( a) |
        1 |
        gis |
        a |
        gis2 a4. r8 |
        cis,4.( b8) cis4.( a8) |
        gis4( b) a( gis) |
        cis4.( b8) cis4.( a8) |
        gis4( b) a( gis) |
        e( eis fis2) |
        gis cis8 r r4 |
        r2 fis8.\f( cis16) fis8 r |
        r2 fis8.( cis16) fis8 r |
        r2 fis8.( cis16) fis8 r |
        r2 fis8.( cis16) fis8 r |
        cis8\f r b r a2 |
        cis8 r b r a2 |
        cis8 r b r a2 |
        cis8\> r b r a2 |
        R1*10\! |
        r2 cis4-.\pp b-. |
        a-. r fis-. e-. |
        cis-. r r2 |
        R1 |
    }
    {% F
        \mark\default
        R1*21 |
        d'4\p( c) d c |
        R1 |
        d4\<( c) d c |
        c8\f r r4 r2 |
        R1*3
    }
    {% G
        \mark\default
        R1*16 |
        a8.\p cis16 4-> a8. cis16 4-> |
        a8.\<-\markup{\italic "cresc. molto"} cis16 4-> a8. cis16 4-> |
        a8. cis16 4-> a8. cis16 4-> |
        a8. cis16 4-> a8. cis16 4-> |
        c1\ff~ |
        1 |
        f,8.\p a16 4-> f8. a16 4-> |
        f8.\<-\markup{\italic "cresc. molto"} a16 4-> f8. a16 4-> |
        f8. a16 4-> f8. a16 4-> |
        f8. a16 4-> f8. a16 4-> |
        c1\ff~ |
        1 |
        R1*4 |
        b4\fz 8-. 8-. 8-. r16 16 8-. 8-. |
        8-.\< 8-. 8-. 8-. 8-. 8-. 8-. 8-. |
        eis1\ff |
        R1 |
        1 |
        R1 |
        r2 gis,4 r |
        r2 4 r |
        a8\f 8 r4 8 8 r4 |
        8\< 8 r4 8 8 r4 |
    }
    {% H
        \mark\default
        c1\ff~ |
        1 |
        4. 16 16 4. 16 16 |
        1 |
        4. 16 16 4. 16 16 |
        1 |
        1\sff |
        1\sff |
        1\sff |
        1\fz |
        fis\fff~ |
        1 |
        4-. 4-. r4 e-. |
        r4 fis-. r8 8-. 4-. |
        r4 4-. r e-. |
        r8 fis8-. 8-. r8 r4 4\fermata \bar "|."
    }
}

% Temp

\new Score{
    \SaxTenor
}
