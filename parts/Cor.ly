\version "2.19.82"

\include "header.ly"

Cor = \relative c'' {
    \StandardConf
    \transposition f
    \tempo "Allegro con fuoco" 4=152
    \clef treble \key b \minor

    {%
        R1*7 |
        r4 d-.\f d-. d-. |
        cis1\< |
    }%
    {% A
        \mark\default
        b2->\ff cis4-. d-. |
        cis4.-> b8 2 |
        b a4 fis8 a |
        b2.-> 4 |
        b2-> cis4-.d |
        cis4.-> b8 2 |
        b4 \tuplet 3/2 {d8 b d} fis4 fis, |
        b2~8 r r4 |

        b2->\fff cis4-. d-. |
        cis4.-> b8 2 |
        b a4 fis8 a |
        b2.-> 4 |
        b2-> cis4-.d |
        cis4.-> b8 2 |
        b4 \tuplet 3/2 {d8 b d} fis4 fis, |
        b2~8 r r4 |
    }
    {% B
        \mark\default
        R1*8 |
        fis4\ff r r2 |
        gis4 r r2 |
        fis4 r fis r |
        fis r r2 |
        fis4 r r2 |
        b2 2 |
        2 4-. ais-. |
        b2 4-. ais-. |
        b4 8-. ais-. b4 8-. ais-. |
        b4 8-. ais-. b-. ais-. b-. ais-. |
    }
    {% C
        \mark\default
        fis1->\ff |
        d4( e fis gis) |
        e2( a8) r r4 |
        fis1-> |
        d4( e fis gis) |
        fis8-. b-. ais-. fis'-. b,-. r r4 |
        R1*4 |
        fis'8 r fis,2. |
        d4( e) fis gis |
        e2( a8) r r4 |
        fis1-^ |
        d'-^ |
        cis |
        eis,8 r r4 r2 |
        R1*5 |
    }
    {% D
        \mark\default
        \tempo 4=120
        R1*18 |
        ais1\p |
        b\< |
        cis->\! |
        d-> |
        fis-> |
        b,2\mf\< c4 b |
        d1\f |
        cis\<-> |
    }
    {% E
        \mark\default
        a->\ff |
        ais-> |
        b4 ais-> b-> gis-> |
        g2-> fis |
        a1-> |
        ais-> |
        b4 ais-> b-> gis-> |
        g2-> fis4. r8 |
        R1*4 |
        d'1-^\f |
        a2 \tuplet 3/2 {d8\< a fis} d r |
        r4\ff fis2->~8 r |
        r4 fis2->~8 r |
        r4 fis2->~8 r |
        r4 fis2->~8 r |
        fis-^\f gis-^ ais-^ fis-^ b-^ fis-^ d-^ b-^ |
        fis'-^ gis-^ ais-^ fis-^ b-^ fis-^ d-^ b-^ |
        a1\p\>~ |
        1\! |
        ais'\mp ~ |
        1 |
        ais\p ~ |
        1 ~ |
        4\pp r r2 |
        R1*9 |
    }
    {% F
        \mark\default
        R1*4 |
        bes2->\f c4-. d-. |
        c4. bes8 2 |
        a4-.\> g-. a-. bes-. |
        a2\p\> g\! |
        fis4 r r2 |
        R1*3 |
        ais2->\fff b4-. cis-. |
        b4. ais8 2 |
        fis2 g4 a |
        g fis fis4.-> 8 |
        b1\fz |
        R1 |
        1\fz |
        R1 |
        1~\fp\> |
        1\pp |
        1~\fp\> |
        1\pp\< |
        d8\f r r4 r2 |
        R1*3 |
    }
    {% G
        \mark\default
        R1*2 |
        gis,1\p ~
        1|
        cis~ |
        2 r |
        R1*2 |
        a1\pp~ |
        1 |
        fis~ |
        1~ |
        4 r r4 4 |
        r2 4 r |
        r4 4 r2 |
        4\p r r4 4 |
        r2 4 r |
        4 r4 4\< r |
        4 r4 4 r |
        4 r4 4 r |
        ais1\ff~ |
        1 |
        4\pp r4 4 r4
        4 r4 4 r4
        4 r4 4 r4
        4 r4 4 r4
        1\ff~ |
        1 |
        4 8 8 8 r16 16 8 8 |
        8 8 8 8 8 8 8 8 |
        b4 8-. 8-. 8-.\< r16 16-. 8-. 8-. |
        8-.\mp\< 8-. 8-. 8-. 8-. 8-. 8-. 8-. |
        e4\fz 8-. 8-. 8-. r16 16-. 8-. 8-. |
        8-.\< 8-. 8-. 8-. 8-. 8-. 8-. 8-. |
        g,,4.\ff bes8-. cis-. e4.->\fz |
        g2 r2 |
        g,4.\ff bes8-. cis-. e4.->\fz |
        g2 r2 |
        r gis4\ff r4 |
        r2 a4 r |
        d8\f 8 r4 8 8 r4 |
        d8\< 8 r4 8 8 r4 |
    }
    {% H
        \mark\default
        d2->\ff e4-. f4-. |
        e4.-> d8 2 |
        2-> c4 a8-. c-. |
        d2.-> r4 |
        2-> c4 a8-. c-. |
        d2.-> r4 |
        d1->\ff |
        c-> |
        c-> |
        b2-> eis,-> |
        b'1->\fff~ |
        1 |
        4-. 4-. r4 4-. |
        r4 4-. r8 8-. 4-. |
        r4 4-. r4 4-. |
        r8 8-. 8-. r8 r4 4\fermata \bar "|."
    }
}

% Temp

\new Score{
    \Cor
}