\version "2.19.82"

\include "header.ly"

ViolonII = \relative c' {
    \StandardConf
    \clef treble \key e \minor
    \tempo "Allegro con fuoco" 4=152

    {%
        b4.\ff( c8) r2 |
        b4.( c8) r2 |
        b4( c8) r b4( c8) r |
        b'8 c16 c b8\< cis16 cis b8 dis16 dis b8 e16 e\! |
        b8 fis'16 fis b,8\< g'16 g b,8 a'16 a b,8 ais'16 ais |
        fis4\ff( g8) r fis4( a8) r |
        fis4( g8) r fis4( ais8) r  |
        r4 <g bes,>-.\f <g bes,>-. <g bes,>-. |
        <fis a,>8\< <fis a,> <fis a,> <fis a,> <fis a,> <fis a,> <fis a,> <fis a,> | 
    }%
    {% A
        \mark\default
        <e, b' g'>4\f r r2 |
        <e c' e>4 r r2 |
        <g b e>4 r <b, d b'> r |
        <g' b e> r r2 |
        <e b' g'>4 r r2 |
        <e c' e>4 r r2 |
        <e b' e>4 r <b' a'> r |
        g'8:16 b: g: fis: e: b: e: g: |

        <e, b' g'>4 r r b'16 e g b |
        <a, fis'>4 r r2 |
        <e b' g'>4 r <e b' g'> r |
        <e b' g'> e16 g b e b4 r |
        <e, b' g'>4 r r b'16 e g b |
        <a, fis'>4 r r2 |
        <e b' e>4 r <b' a'> r |
        g'8:16 b: g: fis: e: b: e: g: |
    }
    {% B
        \mark\default
        fis2-^\ff g4-. a-. |
        g4.-^ fis8 2 |
        dis4-> e-. fis-> e-. |
        <dis fis>2. fis4 |
        fis2-^\ff g4-. a-. |
        g4.-^ fis8 2 |
        dis4-> e-. fis-> e-. |
        <dis fis>2. b4 |

        b,8:16\mf e: g: b: b: g: e: b: |
        c: e: fis: c': c: fis,: e: c: |
        b: e: g: b: b,: e: g: b: |
        b,: e: g: b: b: g: e: b: |
        b: e: g: b: b: g: e: b: |
        e: a: c: e: e,: a: c: e: |
        c: e: c: e: c: e: fis,: b: |
        c: e: c: e: c: e: fis,: b: |
        e: c: c: b: e: c: c: b: |
        e: c: c: b: c: b: c: b: |
    }
    {% C
        \mark\default
        \tuplet 3/2 {e fis g} \tuplet 3/2 {d b d} \tuplet 3/2 {e fis g}  \tuplet 3/2 {d b d} |
        \tuplet 3/2 {e fis g} d'8. c16 b8. g16 fis8. e16 |
        \tuplet 3/2 {d8 fis e} \tuplet 3/2 {d fis e} \tuplet 3/2 {d fis a} \tuplet 3/2 {d a fis} |
        \tuplet 3/2 {e fis g} \tuplet 3/2 {d b d} \tuplet 3/2 {e fis g}  \tuplet 3/2 {d b d} |
        \tuplet 3/2 {e fis g} d'8. c16 b8. g16 fis8. e16 |
        \tuplet 3/2 {e8 fis g} \tuplet 3/2 {fis b b,} \tuplet 3/2 {e fis g} e r |

        fis,8:16\mf dis: g: e: fis: d: e: c: |
        d: b: c: a: b: g: a: dis: |
        fis8:16 dis: g: e: fis: d: e: c: |
        d: b: c: a: b: g: a: fis': |
        <b, b'> r \tuplet 3/2 {d' b d} \tuplet 3/2 {e fis g} \tuplet 3/2 {d b d} |
        \tuplet 3/2 {e fis g} d'8. c16 b8. g16 fis8. e16 |
        \tuplet 3/2 {d8 fis e} \tuplet 3/2 {d fis e} \tuplet 3/2 {d d' b} \tuplet 3/2 {a g fis} |
        \tuplet 3/2 {e fis g} \tuplet 3/2 {d b d} \tuplet 3/2 {e fis g} \tuplet 3/2 {d b d} |
        \tuplet 3/2 {e, fis g} e'8. c16 b8. a16 g8. e16 |
        \tuplet 3/2 {fis8 b d} fis8. e16 d8. c16 b8. fis16 |
        \tuplet 3/2 {g8 bes e} g8. e16 \tuplet 3/2 {e,8 g bes} e8. cis16 |
        \tuplet 3/2 {cis,8 e g} bes8. g16 r4 <g b> |
        r <cis, e>\mf r <cis e> |
        r <cis e>\p r <cis e> |
        R1*2 |
    }
    {% D
        \mark\default
        \tempo 4=120
        cis'1\ppp~ |
        1 |
        a:32\pp |
        cis: |
        b:\< |
        b:\> |
        cis:\pp |
        1: |
        b: |
        1: |
        gis:\pp |
        a4: f: g2: |
        f1: |
        4: e: g: f: |
        1: |
        2: e4: g: |
        f1: |
        2: <f a>: |
        \repeat tremolo 8 {a16 fis16} |
        \repeat tremolo 8 {b16\< g16} |
        \repeat tremolo 8 {c16\p a16} |
        \repeat tremolo 8 {d16\mf b16} |
        \repeat tremolo 6 {b16 g16} b g b a |
        \repeat tremolo 4 {g16\f e16} gis f gis f a e a e |
        \repeat tremolo 4 {g16\mf d16} \repeat tremolo 4 {b'16 g16} |
        \repeat tremolo 8 {fis16\< d16} |
    }
    {% E
        \mark\default
        <b g'>1:16\ff |
        <c fis>: |
        <b g'>2.: e4: |
        <c fis>2: <b g'>: |
        <b g'>1: |
        <c fis>: |
        <b g'>2.: e4: |
        <c fis>2: <b g'>: |
        g16( g') d' b' b( d,) g, g, g( g') d' b' b( d,) g, g, |
        g( g') ees' c' c( ees,) g, g, g( g') ees' c' c( ees,) g, g, |
        g( g') d' b' b( d,) g, g, g( g') d' b' b( d,) g, g, |
        g( g') ees' c' c( ees,) g, g, g( g') ees' c' c( ees,) g, g, |
        \tuplet 3/2 {g8( g') d'} \tuplet 3/2 {ees( g,) g,} \tuplet 3/2 {g8( g') e'} \tuplet 3/2 {cis( e,) g,} |
        g16( d') c' fis fis( c) d, g, g( d') d' g g( d) d, g, |
        <d' d'>8->\f b'-. dis-> b-. e-> b-. b-. r |
        <d, d'>8-> b'-. dis-> b-. e-> b-. b-. r |
        <d, d'>8-> b'-. dis-> b-. e-> b-. b-. r |
        <d, d'>8-> b'-. dis-> b-. e-> b-. b-. r |
        R1*4 |
        d,4:\mp dis: e2: |
        d4: dis: e2: |
        d4:\p dis: e2: |
        d4:\> dis: e2: |
        e1\pp |
        <b g'>1~ |
        1~ |
        8 r r4 r2 |
        g''1:32\ppp |
        1: |
        1: |
        1: |
        1: |
        1: |
    }
    {% F
        \mark\default
        8 r r4 b,16\pp g b g fis a r8 |
        g16 e g e g e r8 fis16 d fis d e c r8 |
        d16 b d b d b r8 c16 a c a b g r8 |
        \repeat tremolo 8 {b16\< g} |
        \repeat tremolo 8 {a16\f\> c} |
        \repeat tremolo 8 {a16\mf\> c} |
        \repeat tremolo 8 {c16\p\> d} |
        \repeat tremolo 8 {c16\! d} |
        b\pp d b d b d g a b g b g fis a r8 |
        g16 e g e g e r8 fis16 d fis d e c r8 |
        d16 b d b d b r8 c16 a c a b g r8 |
        \repeat tremolo 8 {b16\< g} |
        \repeat tremolo 8 {a\f c} |
        \repeat tremolo 8 {a c} |
        \repeat tremolo 8 {b\> a} |
        \repeat tremolo 8 {b\p a} |
        e'1:16\sfp |
        R1 |
        1:\sfp |
        R1 |
        1:\sfp\> |
        R1\! |
        1:\sfp\> |
        R1\! |
        des8\f r r4 r2 |
        R1*3 |
    }
    {% G
        \mark\default
        R1*16 |
        r2 bes4:16\pp c: |
        d:\< e: fis: g: |
        g:\! a: b: c: |
        d:\< e: fis: g: |
        <bes, bes'>1:32\ff |
        1: |
        r2 g,4:16\pp aes: |
        bes:\< c: d: ees: |
        f:\! fis: g: aes: |
        bes:\< c: d: ees: |
        <bes bes'>1:32\ff |
        1: |
        r4 ais,16\pp( b) 8-. ais'16( b) 8-. r4 |
        r4 ais,16( b) 8-. ais'16( b) 8-. r4 |
        r4 dis,16\pp( e) 8-. dis'16\<( e) 8-. r4 |
        r4 dis,16\mp( e) 8-. dis'16\<( e) 8-. r4 |
        r4 dis,16\mf( e) 8-. dis'16\<( e) 8-. r4 |
        r4 dis,16( e) 8-. dis'16( e) 8-. r4 |
        <a,, fis'>1:16\ff |
        a''4->\f b8-.( c-.) b8.( a16) 4 |
        R1 |
        4->\ff b8-.( c-.) b8.( a16) 4 |
        ais->\fz b8-.( cis-.) b8.( ais16) 4 |
        ais->\fz b8-.( cis-.) b8.( ais16) 4 |
        d8 r16 cis16 4 d8 r16 cis16 4 |
        d8\< r16 cis16 4 d8 r16 cis16 4 |
    }
    {% H
        \mark\default
        <bes, g'>2:16\ff 2: |
        2:16 2: |
        2: bes': |
        bes8:16 a: g: f: ees: d: c: bes: |
        2: bes': |
        bes8:16 a: g: f: ees: d: c: bes: |
        bes'8:16 a: g: f: ees: d: c: bes: |
        d': c: bes: a: g: f: ees: d: |
        f: ees: d: c: bes: a: g: f: |
        fis':\< e: d: cis: b: ais: g: fis: |
        b'1:32\fff |
        1: |
        <b, b'>4-. 4-. r4 4-. |
        r4 4-. r8 8-. 4-. |
        r4 4-. r4 4-. |
        r8 8-. 8-. r r4 4\fermata \bar "|."
    }
}

% Temp

\new Score{
    \ViolonII
}