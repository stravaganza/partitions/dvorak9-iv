\version "2.19.82"

\include "header.ly"

Violoncelle = \relative c {
    \StandardConf
    \clef bass \key e \minor
    \tempo "Allegro con fuoco" 4=152

    {%
        b4.\ff( c8) r2 |
        b4.( c8) r2 |
        b4( c8) r b4( c8) r |
        b r16 c( b8\<) r16 cis( b8) r16 dis( b8) r16 e\!( |
        b8\<) r16 fis'( b,8) r16 g'( b,8) r16 a'( b,8) r16 ais' |
        b4\ff( g8) r b4( fis8) r |
        b4( e,8) r b'4( dis,8) r |
        r4 ees-.\f des-. des-. |
        c8\< b c b c b c' b |
    }%
    {% A
        \mark\default
        e,4\ff r r2 |
        <a, e'>4 r r2 |
        e'4 r b r |
        e4 r r2 |
        e4 r r2 |
        a,4 r r2 |
        g4 r fis r |
        e8 e' e e e r r4 |

        e4 r r b16 e g b |
        a4 r r2 |
        e4 r g r |
        e e16 g b e b4 r |
        e, r r b16 e g b |
        a4 r r2 |
        g4 r8 g, fis4 r8 fis' |
        e e e e e r r4 |
    }% End A
    {% B
        \mark\default
        b'4-.\ff a-. g-. fis-. |
        g2-^ a4-. b-. |
        b-. c-. d e8 c8 |
        b4-> a8-. fis-. b4-. b,-. |
        b'4-> a-> g-> fis-> |
        g2-> a4-. b-. |
        b-. c-. d e8 c8 |
        b4-> a8-. fis-. b4-. b,-. |

        e2 r |
        a, r |
        e'4 r8 e g4 r8 g |
        e2 r4 g8 e |
        e2 r4 e |
        c2 g |
        a-> a4-. b-. |
        a2-> a4-. b-. |
        a4-> 8-. b-. a4-> 8-. b-. |
        a4-> 8-. b-. a8-. b-. a8-. b-. |
    }
    {% C
        \mark\default
        e-. r b-. r e-. r b-. r |
        e,-. r fis-. r g-. r a-. r |
        d, r d'8. 16 4 r |
        e8-. r b-. r e-. r b-. r |
        e,-. r fis-. r g-. r a-. r |
        b-. r8 8 r e-. r \tuplet 3/2 {e g a} |
        b r b, r r2 |
        b'8 r b, r r2 |
        b'8 r b, r r2 |
        b'8 r b, r r2 |
        e8-. r b-. r e-. r b-. r |
        e,-. r fis-. r g-. r a-. r |
        d, r d'8. 16 4 r |
        e8-. r b-. r e-. r b-. r |
        e1 |
        d |
        cis8 r cis4-- r bes-- |
        r g-- r g-- |
        R1*4 | 
    }
    {% D
        \mark\default
        \tempo 4=120
        g''1\ppp~ |
        1~ |
        4 r r2 |
        R1 |
        r2 r4 \tuplet 3/2 {d,8\mf\<( g) b} |
        d8.\! b16( d8.) a16( d8.) g,16 r4 |
        R1*2 |
        r2 r4 \tuplet 3/2 {d8\<( g) b} |
        d8.\! b16( d8.) a16( d8.) g,16 r4 |
        R1*2 |
        r4 \tuplet 3/2 {c,8\mf( f) a} c8. a16\<( c8.) g16( |
        c8.)\! f,16 r4 r2\> |
        R1*2\!
        r4 \tuplet 3/2 {c8\mf( f) a} c8. a16\<( c8.) g16( |
        c8.)\! f,16 r4 r2\> |
        r4 \tuplet 3/2 {dis,8\p( fis) g} a8.( fis16-.) a8.( fis16-.) |
        r4 \tuplet 3/2 {e8 g b} e4 r\< |
        r\! \tuplet 3/2 {fis,8\p( a) b} c8.( a16-.) c8.( a16-.) |
        r4 \tuplet 3/2 {g8 b d} g4 r |
        \tuplet 3/2 {b,8 d g} b4 dis\f( b8) r |
        c4-.\< c,-. r c-. |
        r\! \tuplet 3/2 {d8 g b} d8. b16 d8. d,16 |
        r4\< \tuplet 3/2 {d8 g b} d8. b16 d8. d,16 |
    }
    {% E
        \mark\default 
        g4\ff g, r g |
        g'8-> g g,4 r g |
        g'-. g,-. g'-. g,-. |
        g'-. g,-. g'-. g,-. |
        g'4 g, r g |
        g'8-> g g,4 r g |
        g'-. g,-. g'-. g,-. |
        g'-. g,-. g'-. g,-. |
        g'4.->\f g,-> g'4->~ |
        8 g,4.-> g'4.-> g,8->~ |
        4 g'4.-> g,-> |
        g'-> g,-> g'4-> |
        g,-- g'-- g,-- g'-- |
        g,-- g'-- g,-- g'-- |
        b8 r a r g2 |
        b8 r a r g2 |
        b8 r a r g2 |
        b8 r a r g2 |
        R1*2 |
        <d' fis>8-"pizz."\mf d, <c' e> d, <b' d> d, <b' d> d, |
        <d' fis>8\> d, <c' e> d, <b' d> d, <b' d> d, |
        b'-.-"arco"\mp b-. a-. a-. g-. g-. g-. a-. |
        b16-. b-. 8-. a-. a-. g-. 16-. g-. 8-. a-. |
        b16-.\p b-. 8-. a-. a-. g-. g-. g-. a-. |
        b16-. b-. 8-. a-. a-. g-. 16-. g-. 8-. a-. |
        g1\pp |
        g,~ |
        1~ |
        8 r r4 r2 |
        R1*2 |
        r2 b'4-.-"pizz." a-. |
        g-. r e-. d-. |
        b-. r r2 |
        R1 |
    }
    {% F
        \mark\default
        b'16\pp g b g b g b g b8 r fis16 a fis a |
        e g r8 e16 g e g d fis r8 c16 e c e |
        b d r8 b16 d b d a c r8 g16 b g b |
        \repeat tremolo 8 {b\< g} |
        \repeat tremolo 8 {c\f\> a} |
        \repeat tremolo 8 {c\mf\> a} |
        \repeat tremolo 8 {d\p\> g,} |
        \repeat tremolo 8 {d'\! g,} |
        \repeat tremolo 4 {d'\pp g,} g'8 r fis16 a fis a |
        e g r8 e16 g e g d fis r8 c16 e c e |
        b d r8 b16 d b d a c r8 g16 b g b |
        \repeat tremolo 8 {b\< g} |
        \repeat tremolo 8 {a\f fis} |
        \repeat tremolo 8 {a fis} |
        \repeat tremolo 8 {a\> fis} |
        \repeat tremolo 8 {a\p fis} |
        g4\pp a8-. b-. a8. g16 g8 r |
        R1 |
        g4 a8-. b-. a8. g16 g8 r |
        R1 |
        bes4\pp c8-. d-. c8. bes16 bes8 r |
        R1 |
        bes4\pp c8-. d-. c8. bes16 bes8 r |
        R1 |
        bes'8\f r r4 r2 |
        \tuplet 3/2 {bes8 c des} \tuplet 3/2 {g, a bes} \tuplet 3/2 {e, f g} \tuplet 3/2 {c, d e} |
        f r f,2.\fp~ |
        1~ |
    }
    {% G
        \mark\default
        1~ |
        1~ |
        2 r4 \tuplet 3/2 {cis'8\p 8 8} |
        f,4-. 4-. r2 |
        fis1\pp~ |
        1~ |
        1~ |
        1~ |
        2 r4 \tuplet3/2 {d'8 8 8} |
        fis,4-. 4-. r2 |
        g1~ |
        1~ |
        1~ |
        1~ |
        1~ |
        1 |
        4-. 4-. r4 4 |
        r4\< 4 r4 4 |
        r4\! 4 r4 4 |
        r4\< 4 r4 4 |
        1:16\ff |
        1: |
        4-.\pp 4-. r4 4 |
        r4\< 4 r4 4 |
        r4\! 4 r4 4 |
        r4\< 4 r4 4 |
        fis1:\ff |
        1: |
        fis2\pp g4 a |
        g4. fis8 2 |
        g2 a4\< b |
        a4.\mp\< g8 2 |
        a2\mf\< b4 c |
        b4.\f\< a8 2 |
        c4.\ff ees8-. fis8-. a4.-> |
        c2->\fz r |
        c,4.\ff ees8-. fis8-. a4.-> |
        c2->\fz r |
        cis,8\ff e cis' r r2 |
        d,8 fis d' r r2 |
        e,8 e' r4 e,8 e' r4 |
        e,8\< e' r4 e,8 e' r4 |
    }
    {% H
        \mark\default
        d,2:16\ff 2: |
        2:16 2: |
        2: d'2: |
        d,1: |
        2: d': |
        d,1: |
        2:16  2: |
        2:16  2: |
        2:16  2: |
        cis:\< fis,: |
        b1:32\fff |
        1: |
        4-. 4-. r4 4-. |
        r4 4-. r8 8-. 4-. |
        r4 4-. r4 4-. |
        r8 8-. 8-. r r4 4\fermata \bar "|."
    }
}

% Temp

\new Score{
    \Violoncelle
}