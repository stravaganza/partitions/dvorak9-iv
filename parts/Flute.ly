\version "2.19.82"

\include "header.ly"

Flute = \relative c''' {
    \StandardConf
    \clef treble \key e \minor
    \tempo "Allegro con fuoco" 4=152

    {%
        R1*7 |
        r4 dis-.\f dis-. dis-. |
        dis1\prall\trill\< |
    }%
    {% A
        \mark\default
        e4\f r r2 |
        c4 r r2 |
        b4 r b r |
        b r r2 |
        b4 r r2 |
        c4 r r2 |
        b4 r dis r |
        e2 ~ 8 r r4 |
        e r r2 |
        e4 r r2 |
        b4 r b r |
        b r r2 |
        e4 r r2 |
        e4 r r2 |
        b4 r dis r |
        e2 ~ 8 r r4 |
    }% end A
    {% B
        \mark\default
        R1*7 |
        r2 r4 b4\ff |
        e2-> fis4-. g-. |
        fis4.-> e8 2 |
        e d4 b8 d |
        e2.-> 4 |
        e2-> fis4-. g-. |
        a4. g16 a b2 |
        g16 fis e4. fis4-. dis-. |
        g16 fis e4. fis4-. dis-. |
        g16( fis) e8 fis-. dis-. g16( fis) e8 fis-. dis-. |
        g16( fis) e8 fis-. dis-. fis-. dis-. fis-. dis-. |
    }% End B
    {% C
        \mark\default
        e\f r d r e r d r |
        e r d r e r a, r |
        a r d r d r r4 |
        e8 r d r e r d r |
        e r d r d r c r |
        e r dis r e r r4 |

        \tuplet 3/2 {b8\( c d} \tuplet 3/2 {c g c} \tuplet 3/2 {b c b} \tuplet 3/2 {a g a} |
        \tuplet 3/2 {g fis g} \tuplet 3/2 {fis e fis} \tuplet 3/2 {e fis e} \tuplet 3/2 {dis e b'\)} |
        \tuplet 3/2 {b8\( c d} \tuplet 3/2 {c g c} \tuplet 3/2 {b c b} \tuplet 3/2 {a g a} |
        \tuplet 3/2 {g fis g} \tuplet 3/2 {fis e fis} \tuplet 3/2 {e fis e} \tuplet 3/2 {dis e b'\)} |

        e8 r d r e r d r |
        e r d r b r a r |
        a r d r d r r4 |
        b8 r d r g r d r |
        b4 r r2 |
        fis'4 r r2 |

        \tuplet 3/2 {e8 dis e} r4 \tuplet 3/2 {e8 dis e} r4 |
        \tuplet 3/2 {bes8 a bes} r4 \tuplet 3/2 {e,8 dis e} r4 |
        r4 \tuplet 3/2 {bes8\mf a bes} r4 \tuplet 3/2 {bes8 a bes} |
        r4 \tuplet 3/2 {bes8\p a bes} r4 \tuplet 3/2 {bes8 a bes} |
        des\>( c) des c des c des c |
        des1~\pp |
    }
    {% D
        \mark\default
        \tempo 4=120
        8 r r4 r2 |
        R1*11 |
        r2 r4 f,~ |
        4\< e g f |
        2\! r2 |
        R1*2 |
        a2\pp^"2 ou 3" 4.\< 8 |
        c2.\p b4 |
        4\< g' fis e |
        2.\mp d4 |
        4\mf b' a g |
        d' b a g |
        fis'\< e d c |
        b2\f( d) |
        b\<( a) |
    }
    {% E
        \mark\default
        g4.\ff( b8) a4. g8 |
        a4.( c8) b4. a8 |
        b-> b b-> b b-> b b-> b |
        b r a r g2 |
        4.( b8) a4. g8 |
        a4.( c8) b4. a8 |
        \acciaccatura ais b-. b-. b-. b-. b-. b-. \acciaccatura ais b-. b-. |
        b r a r g4. r8 |
        <g g'>4.( <fis fis'>8) <g g'>4.( <b, b'>8) |
        a'4( c) b( a) |
        <g g'>4.( <fis fis'>8) <g g'>4.( <b, b'>8) |
        a'4( c) b( a) |
        d( dis) e2 |
        d\prall\trill <g, g'>8 r r4 |
        R1*4 |
        b8\f r a r g2 |
        b8 r a r g2 |
        b8 r a r g2 |
        b8\> r a r g2 |
        b1\mp~ |
        1~ |
        1~\p |
        1~ |
        4\pp r r2 |
        R1*9 |
   }
   {% F
        \mark\default
        r2 <b b'>8\pp r <a a'> r |
        <g g'>2\prall\trill <fis fis'>8 r <e e'> r |
        <d d'>2\prall\trill <c c'>4( <b b'>) |
        a'\< b c8( b) e d |
        c1~\f |
        1 |
        b~\p |
        2( a) |
        g8 r r4 <b b'>8\pp r <a a'> r |
        <g g'>2\prall\trill <fis fis'>8 r <e e'> r |
        <d d'>2\prall\trill <c c'>4( <b b'>) |
        a'\< b c8( b) e d |
        c1~\f |
        1 |
        b1~\p |
        1~ |
        8 r r4 r2 |
        \tuplet 3/2 {bes,8\fz\> 8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8\!} |
        R1 |
        \tuplet 3/2 {8\fz\> 8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8\!} |
        R1 |
        \tuplet 3/2 {8\fz\> 8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8\!} |
        R1 |
        \tuplet 3/2 {8\p\< 8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8} |
        des\f r r4 r2 |
        R1*3 |
   }
   {% G
        \mark\default
        a'4.\p\< c8 2 |
        a4.\! g8 f2\> |
        2 g4 a |
        g4.\! f8 2 |
        ais1\pp~ |
        2 r |
        ais4.\pp cis8 2 |
        ais4. gis8 fis2 |
        2 gis4 ais |
        gis4. fis8 2 |
        b1~ |
        4. r8 r2 |
        b4.\p( d8) 2 |
        b4.( a8) g2 |
        b8 r16 d16 4-. b8 r16 a16 g4-. |
        R1 |
        b8.\p d16 4-> b8. d16 4-> |
        b8.\<-\markup{\italic "cresc. molto"} d16 4-> b8. d16 4-> |
        b8. d16 4-> b8. d16 4-> |
        b8. d16 4-> b8. d16 4-> |
        ees1\ff~ |
        1 |
        g,8.\p bes16 4-> g8. bes16 4-> |
        g8.\<-\markup{\italic "cresc. molto"} bes16 4-> g8. bes16 4-> |
        g8. bes16 4-> g8. bes16 4-> |
        g8. bes16 4-> g8. bes16 4-> |
        ees1\ff~ |
        1 |
        b4\p cis8-. dis-. cis-. r16 b16 8-. dis-. |
        cis-. b-. dis-. cis-. b-. dis-. cis-. b-. |
        <e, e'>4\fp  <fis fis'>8-. <g g'>-. <fis fis'>-.\< r16 <e e'>16 8-. <g g'>-. |
        <fis fis'>-.\mp\< <e e'>-. <g g'>-. <fis fis'>-. <e e'>-. <g g'>-. <fis fis'>-. <e e'>-. |
        <fis fis'>4->\fz <g g'>8-. <a a'>-. <g g'>-. r16 <fis fis'>16 8-. <a a'>-. |
        <g g'>-.\< <fis fis'>-. <a a'>-. <g g'>-. <fis fis'>-. <a a'>-. <g g'>-. <fis fis'>-. |
        <fis fis'>1\ff |
        R1 |
        1 |
        R1 |
        r2 4 r |
        r2 4 r |
        bes8\f 8 r4 8 8 r4 |
        8\< 8 r4 8 8 r4 |
   }
   {% H
        \mark\default
        d1\ff~ |
        1 |
        4. 16 16 4. 16 16 |
        1 |
        4. 16 16 4. 16 16 |
        1 |
        1\sff |
        1\sff |
        1\sff |
        e\fz |
        <g, g'>\fff~ |
        1 |
        4-. 4-. r4 4-. |
        r4 4-. r8 8-. 4-. |
        r4 4-. r4 4-. |
        r8 8-. 8-. r8 r4 4\fermata \bar "|."
   }
}

% Temp

\new Score{
    \Flute
}