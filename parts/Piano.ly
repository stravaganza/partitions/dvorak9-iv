\version "2.19.82"

\include "header.ly"

PianoD = \relative c'' {
    \clef treble \key e \minor
    {%
        R1*8 |
        r8\< <b b,> r  <b b,> r <b b,> r <b b,>
    }%
    { % A
        \mark\default
        <e, g>4\ff r r2 |
        <e e'>4 r r2 |
        <e g>4 r <b g'> r |
        <b g'> r r2 |
        <b g'>4 r r2 |
        <e e'>4 r r2 |
        <e g>4 r <dis a'> r |
        <e g>2~8 r r4 |

        <b' g'> r r2 |
        <e, e'>4 r r2 |
        <b' g'>4 r <b g'> r |
        <b g'>4 r r2 |
        <b g'>4 r r2 |
        <e, e'>4 r r2 |
        <b' g'>4 r <dis a'> r |
        <b g'>2~8 r r4 |
    }
    {% B
        \mark\default
        R1*8 |
        <b, g'>4\ff r r2 |
        <c fis>4 r r2 |
        <b g'>4 r <b g'> r |
        <b g'> r r2 |
        <b g'>4 r r2 |
        <e a>2 <e b'> |
        <e g> <e fis>4-. <dis fis>-. |
        <e g>2 <e fis>4-. <dis fis>-. |
        <e g>4 <e fis>8-. <dis fis>-. <e g>4 <e fis>8-. <dis fis>-. |
        <e g>4 <e fis>8-. <dis fis>-. <e fis>-. <dis fis>-. <e fis>-. <dis fis>-. |
    }
    {% C
        \mark\default
        r8\f g r d r g r d |
        r g r a r b r cis |
        r <fis, a> r <fis a>8 8 r r4 |
        c'8 g b d, c' g b d, |
        c' g r a r b c c |
        r g r fis-. e-. r r4 |
        R1*4 |
        c'8 g b d, c' g b d, |
        c' g r a r b cis d |
        r g, r fis-. e-. r r4 |
        e8 g b, d e g b, d |
        e g r a r b a c |
        b, g' r fis-. e-. r r4 |
        \tuplet 3/2 {bes''8 8 8} r4 \tuplet 3/2 {e8 dis e} r4 |
        \tuplet 3/2 {bes8 a bes} r4 \tuplet 3/2 {e,8 des e} r4 |
        R1*4 |
    }
    {% D
        \mark\default
        R1*26 |
    }
    {% E
        \mark\default
        <b g'>4.\ff( b'8) a4.( g8) |
        a4.( c8) b4.( a8) |
        b-> b b-> b b-> b b-> b |
        <b, b'> r <a a'> r <g g'>2 |
        <b g'>4.\ff( b'8) a4.( g8) |
        a4.( c8) b4.( a8) |
        b-> b b-> b b-> b b-> b |
        <b, b'> r <a a'> r <g g'>2 |
        <b g'>4.( <a fis'>8) <b g'>4.( <g b>8) |
        <fis a>4( <a c>) <g b>( <fis a>) |
        <b g'>4.( <a fis'>8) <b g'>4.( <g b>8) |
        <fis a>4( <a c>) <g b>( <fis a>) |
        <b, b'>2 <b b'> |
        fis' g8 r r4 |
        R1*4 |
        <b b'>8\f r <a a'> r <g g'>2 |
        <b b'>8 r <a a'> r <g g'>2 |
        <b b'>8 r <a a'> r <g g'>2 |
        <b b'>8\> r <a a'> r <g g'>2 |
        R1*14\!
    }
    {% F
        \mark\default
        R1*28 |
    }
    {% G
        \mark\default
        R1*20 |
        <g g'>4.\ff <des' bes'>8 8 r r4 |
        <g, g'>4. <f f'>8 <ees ees'>2 |
        R1*4 |
        <g g'>4.\ff <des' bes'>8 8 r r4 |
        <g, g'>4. <f f'>8 <ees ees'>2 |
        <a' b>4\p <a cis>8-. <a dis>-. <a cis>-. r16 <a b>16 8-. <a dis>-. |
        <a cis>-. <a b>-. <a dis>-. <a cis>-. <a b>-. <a dis>-. <a cis>-. <a b>-. |
        <b e>4\fp <b fis'>8-. <b g'>-. <b fis'>-.\< r16 <b e>16 8-. <b g'>-. |
        <b fis'>-.\mp\< <b e>-. <b g'>-. <b fis'>-. <b e>-. <b g'>-. <b fis'>-. <b e>-. |
        <c fis>4\fz-> <c g'>8-. <c a'>-. <c g'>-. r16 <c fis>16 8-. <c a'>-. |
        <c g'>-.\< <c fis>-. <c a'>-. <c g'>-. <c fis>-. <c a'>-. <c g'>-. <c fis>-. |
        <dis fis>1\ff-> |
        R1 |
        1 |
        R1 |
        r2 <e, fis>4-> r |
        r2 <b fis'>4-> r |
        bes'8\f 8 r4 8 8 r4 |
        8\< 8 r4 8 8 r4 |
    }
    {% H
        \mark\default
        <g bes>1\ff~ |
        1 |
        <d bes'>4. 16 16 4. 16 16 |
        1 |
        4. 16 16 4. 16 16 |
        1 |
        <g bes>1\sff |
        <f bes>\sff |
        1\sff |
        <e ais>\fz |
        b,->\fff~ |
        1 |
        <e' g>4-. 4-. r <d g>-. |
        r <e g>-. r8 8-. 4-. |
        r <e g>-. r <d g> |
        r8 <e g>8-. 8-. r r4 4\fermata \bar "|."
    }
}

PianoL = \relative c' {
    \StandardConf
    \clef bass \key e \minor
    \tempo "Allegro con fuoco" 4=152
    {%
        R1*7 |
        r4\f <ees g,>-. <des g,>-. <des g,>-. |
        c8\< b c b c b c b |
    }%
    {% A
        <g b>4\! r r2 |
        <e a c>4 r r2 |
        <e g b>4 r <g b> r |
        <e g b> r r2 |
        <e g b>4 r r2 |
        <e a c>4 r r2 |
        <e g b>4 r <b fis' a> r |
        <g' b>2~8 r r4 |

        <e g b>4 r r2 |
        <e a c>4 r r2 |
        <e g b>4 r8 e <g b>4 r |
        <e g b> r r2 |
        <e g b>4 r r2 |
        <e a c>4 r r2 |
        <e b'>4  r8 e <a b dis>4 r8 b |
        <b e>2~8 r r4 |
    }
    {% B
        R1*8 |
        <g, b e>4\ff r r2 |
        <a c e>4 r r2 |
        <g b e>4 r <g b> r |
        <g b e> r r2 |
        <g b e>4 r r2 |
        <c e>2-> <g b e>-> |
        <a c e>2 4-. <fis b>-. |
        <a c e>2 e' |
        <a, c>4 8-. <fis b>-. <a c>4 8-. <fis b>-. |
        <a c>4 8-. <fis b>-. <a c>-. <fis b>-. <a c>-. <fis b>-. |
    }
    {% C
        e'8\f r b r e r b r |
        <e, g> r <fis a> r <g b> r <a des> r |
        <d, d'> r <d d'> r <d d'> r r4 |
        e'8 r b r e r b r |
        <e, g> r <fis a> r <g b> r <a c> r |
        <g b> r <fis ees'> r <g e'> r r4 |
        R1*4 |
        e'8 r b' r e, r b' r |
        <e, g> r <fis a> r <g b> r <a cis> r |
        <d, d'> r <d d'> r <d d'> r r4 |
        e8 r b' r e, r b' r |
        <g b> r r4 r2 |
        <fis b>8 r r4 r2 |
        r4 <e e'> r <e e'> |
        r <e e'> r <e e'> |
        R1*4 |
    }
    {
        \tempo 4=120
        R1*2 |
        r4 d8\pp 8 r2 |
        R1*3 |
        r4 d8 8 r2 |
        R1*19 |
    }
    {% E
        g,8-> 8-> 8-> 8-> 2 |
        8-> 8-> 8-> 8-> 2 |
        8-> 8-> 8-> 8-> 2 |
        8-> 8-> 8-> 8-> 2 |
        8-> 8-> 8-> 8-> 2 |
        8-> 8-> 8-> 8-> 2 |
        8-> 8-> 8-> 8-> 2 |
        8-> 8-> 8-> 8-> 2 |
        1 |
        1 |
        1 |
        1 |
        4 4 4 4 |
        4 4 8 r r4 |
        R1*22
    }
    {% F
        R1*28 |
    }
    {% G
        R1*20 |
        <e' e'>1\ff~ |
        1 |
        R1*4 |
        1~ |
        1 |
        b'4 8 8 8 r16 16 8 8 |
        8 8 8 8 8 8 8 8 |
        4 8-. 8-. 8-.\< r16 16-. 8-. 8-. |
        8-.\mp\< 8-. 8-. 8-. 8-. 8-. 8-. 8-. |
        e4\fz 8-. 8-. 8-. r16 16-. 8-. 8-. |
        8-.\< 8-. 8-. 8-. 8-. 8-. 8-. 8-. |
        fis1\ff-> |
        R1 |
        <fis, fis'> |
        R1 |
        r2 4-> r |
        r2 4-> r |
        <e e'>8\f 8 r4 8 8 r4 |
        8\< 8 r4 8 8 r4 |
    }
    {% H
        <d d'>1-^\ff~ |
        1 |
        <d bes>4. 16 16 4. 16 16 |
        1 |
        4. 16 16 4. 16 16 |
        1 |
        1-> |
        1-> |
        1-> |
        <cis cis'>2-> fis-> |
        <b,, b'>1->\fff~ |
        1 |
        b''4-. 4-. r4 4-. |
        r4 4-. r8 8-. 4-. |
        r4 4-. r4 4-. |
        r8 8-. 8-. r r4 4\fermata \bar "|."
    }
}

Piano = \new PianoStaff <<
        \PianoL
        \PianoD
    >>

% Temp

\new Score{
    \Piano
}
