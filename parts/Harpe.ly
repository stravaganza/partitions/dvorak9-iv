\version "2.19.82"

\include "header.ly"

HarpeD = \relative c'' {
    \clef treble \key e \minor
    {%
        R1*9 |
    }%
    {% A
        \mark\default
        <e, g>4\ff r r2 |
        <e e'>4 r r2 |
        <e g>4 r <b g'> r |
        <b g'> r r2 |
        <b g'>4 r r2 |
        <e e'>4 r r2 |
        <e g>4 r <dis a'> r |
        <e g>2~8 r r4 |

        <b' g'> r r2 |
        <e, e'>4 r r2 |
        <b' g'>4 r <b g'> r |
        <b g'>4 r r2 |
        <b g'>4 r r2 |
        <e, e'>4 r r2 |
        <b' g'>4 r <dis a'> r |
        <b g'>2~8 r r4 |
    }
    {% B
        \mark\default
        R1*8 |
        <b, g'>4\ff r r2 |
        <c fis>4 r r2 |
        <b g'>4 r <b g'> r |
        <b g'> r r2 |
        <b g'>4 r r2 |
        <e a>2 <e b'> |
        <e g> <e fis>4-. <dis fis>-. |
        <e g>2 <e fis>4-. <dis fis>-. |
        <e g>4 <e fis>8-. <dis fis>-. <e g>4 <e fis>8-. <dis fis>-. |
        <e g>4 <e fis>8-. <dis fis>-. <e fis>-. <dis fis>-. <e fis>-. <dis fis>-. |
    }
    {% C
        \mark\default
        r8\f g r d r g r d |
        r g r a r b r cis |
        r <fis, a> r8 8 8 r r4 |
        c'8 g b d, c' g b d, |
        c' g r a r b c c |
        r g r fis-. e-. r r4 |
        g\mf b,8-> r r2 |
        g'4 b,8-> r r2 |
        g'4 b,8-> r r2 |
        g'4 b,8-> r r2 |
        e'8\p r b' r e, r b' r |
        <e, g> r <fis a> r <g b> r <a cis> r |
        <d, d'> r <d d'> r <d d'> r r4 |
        e8 r b' r e, r b' r |
        <g b> r r4 r2 |
        <fis b>8 r r4 r2 |
        r4 <e e'> r <e e'> |
        r <e e'> r <e e'> |
        r <cis e>\mf r <cis e>|
        r <cis e>\p r <cis e> |
        R1*2 |
    }
    {
        \mark\default
        R1*26
    }
    {% E
        \mark\default
        <b g'>4.\ff( b'8) a4.( g8) |
        a4.( c8) b4.( a8) |
        b-> 8 8-> 8 8-> 8 8-> 8 |
        <b, b'> r <a a'> r <g g'>2 |
        <b g'>4.\ff( b'8) a4.( g8) |
        a4.( c8) b4.( a8) |
        b-> 8 8-> 8 8-> 8 8-> 8 |
        <b, b'> r <a a'> r <g g'>2 |
        fis,16( g') d' b' b( d,) g, g, g( g') d' b' b( d,) g, g, |
        g( g') ees' c' c( ees,) g, g, g( g') ees' c' c( ees,) g, g, |
        g( g') d' b' b( d,) g, g, g( g') d' b' b( d,) g, g, |
        g( g') ees' c' c( ees,) g, g, g( g') ees' c' c( ees,) g, g, |
        \tuplet 3/2 {g8( g') d'} \tuplet 3/2 {ees( g,) g,} \tuplet 3/2 {g8( g') e'} \tuplet 3/2 {cis( e,) g,} |
        g16( d') c' fis fis( c) d, g, g( d') d' g g( d) d, g, |
        R1*4 |
        <b' b'>8\f r <a a'> r <g g'>2 |
        <b b'>8 r <a a'> r <g g'>2 |
        <b b'>8 r <a a'> r <g g'>2 |
        <b b'>8\> r <a a'> r <g g'>2\! |
        R1*14 |
    }
    {% F
        \mark\default
        R1*28 |
    }
    {% G
        \mark\default
        R1*20 |
        <g g'>4.\ff <des' bes'>4 r8 r4 |
        <g, g'>4. <f f'>8 <ees ees'>2 |
        R1*4 |
        <g g'>4.\ff <des' bes'>4 r8 r4 |
        <g, g'>4. <f f'>8 <ees ees'>2 |
        b'4\mf cis8-. dis-. cis-. r16 b16 8-. dis-. |
        cis-. b-. dis-. cis-. b-. dis-. cis-. b-. |
        e4 fis8-. g-. fis-.\< r16 e16 8-. g-. |
        fis8-.\f\< e8-. g8-. fis8-. e8-. g8-. fis8-. e8-. |
        fis4\sff-> g8-. a-. g-. r16 fis16 8-. a8-. |
        g-.\< fis-. a-. g-. fis-. a-. g-. fis-. |
        R1*8\! |
    }
    {% H
        \mark\default
        R1*16 |
        \bar "|."
    }
}

HarpeL = \relative c' {
    \StandardConf
    \clef bass \key e \minor
    \tempo "Allegro con fuoco" 4=152
    {%
        R1*9 |
    }%
    {% A
        <g b>4\! r r2 |
        <e a c>4 r r2 |
        <e g b>4 r <g b> r |
        <e g b> r r2 |
        <e g b>4 r r2 |
        <e a c>4 r r2 |
        <e g b>4 r <b fis' a> r |
        <g' b>2~8 r r4 |

        <e g b>4 r r2 |
        <e a c>4 r r2 |
        <e g b>4 r8 e <g b>4 r |
        <e g b> r r2 |
        <e g b>4 r r2 |
        <e a c>4 r r2 |
        <e b'>4  r8 e <a b dis>4 r8 b |
        <b e>2~8 r r4 |
    }
    {% B
        R1*8 |
        <g, b e>4\ff r r2 |
        <a c e>4 r r2 |
        <g b e>4 r <g b> r |
        <g b e> r r2 |
        <g b e>4 r r2 |
        <c e>2-> <g b e>-> |
        <a c e>2 4-. <fis b>-. |
        <a c e>2 e' |
        <a, c>4 8-. <fis b>-. <a c>4 8-. <fis b>-. |
        <a c>4 8-. <fis b>-. <a c>-. <fis b>-. <a c>-. <fis b>-. | 
    }
    {% C
        e'8\f r b r e r b r |
        <e, g> r <fis a> r <g b> r <a des> r |
        <d, d'> r <d d'> r <d d'> r r4 |
        e'8 r b r e r b r |
        <e, g> r <fis a> r <g b> r <a c> r |
        <g b> r <fis ees'> r <g e'> r r4 |
        g'\mf b,8-> r r2 |
        g'4 b,8-> r r2 |
        g'4 b,8-> r r2 |
        g'4 b,8-> r r2 |
        e8\p r b' r e, r b' r |
        <e, g> r <fis a> r <g b> r <a cis> r |
        <d, d'> r <d d'> r <d d'> r r4 |
        e8 r b' r e, r b' r |
        <g b> r r4 r2 |
        <fis b>8 r r4 r2 |
        r4 <e e'> r <e e'> |
        r <e e'> r <e e'> |
        r <cis e>\mf r <cis e> |
        r <cis e>\p r <cis e> |
        R1 |
        r8 g-.\mp bes-. cis-. e-.\< g-. \tuplet 3/2 {bes-.( cis-.) e-.} |
    }
    {% D
        \tempo 4=120
        4\! r r2 |
        R1*25 |
    }
    {% E
        d,,1->\ff
        dis-> |
        e4 dis-> e-> cis-> |
        c2-> b |
        d1-> |
        dis-> |
        e4 dis-> e-> cis-> |
        c2-> b4. r8 |
        R1*28 |
    }
    {% F
        R1*28 |
    }
    {% G
        R1*20 |
        <e' e'>1\ff |
        1 |
        R1*4 |
        1 |
        1 |
        fis,2\pp g4 a |
        g4. fis8 2 |
        g a4\< b |
        a4.\mp\< g8 2 |
        a\mf\< b4 c |
        b4.\f\< a8 2 |
        c4.\ff ees8-. fis-. a4.\fz-> |
        c2 r |
        c,4.\ff ees8-. fis-. a4.\fz-> |
        c2 r |
        cis,8 e cis' r r2 |
        d,8 fis d' r r2 |
        e,8 e' r4 e,8 e' r4 |
        e,8\< e' r4 e,8 e' r4 |
    }
    {% H
        R1*16\!
        \bar "|."
    }
}

Harpe = \new PianoStaff <<
        \HarpeL
        \HarpeD
    >>

% Temp

\new Score{
    \Harpe
}