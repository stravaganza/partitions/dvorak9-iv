\version "2.19.82"

\include "header.ly"

Timbale = \relative c {
    \StandardConf
    \tempo "Allegro con fuoco" 4=152
    \clef bass \key e \minor

    {%
        R1*8 |
        a1:32\mf\< |
    }%
    { % A
        \mark\default
        R1\ff |
        R1*6 |
        e'8:16\mf e: e: e: e r r4 |
        R1*7 |
        e8: e: e: e: e r r4 |
    }
    {% B
        \mark\default
        R1*14 |
        e,2 4-. dis'-. |
        e,2 4-. dis'-. |
        e,4 8-. dis'-. e,4 8-. dis'-. |
        e,4 8-. dis'-. e,-. dis'-. e,-. dis'-. |
    }
    {% C
        \mark\default
        R1*22
    }
    {% D
        \mark\default
        \tempo 4=120
        R1*26
    }
    {% E
        \mark\default
        g,8-> 8-> 8-> 8-> 2 |
        8-> 8-> 8-> 8-> 2 |
        8-> 8-> 8-> 8-> 2 |
        8-> 8-> 8-> 8-> 2 |
        8-> 8-> 8-> 8-> 2 |
        8-> 8-> 8-> 8-> 2 |
        8-> 8-> 8-> 8-> 2 |
        8-> 8-> 8-> 8-> 2 |
        4->\ff r r g-> |
        r2 g4-> r |
        r g-> r2 |
        g4-> r r g-> |
        g1:32\pp\< |
        1:\f\> |
        R1*22\pp |
    }
    {% F
        \mark\default
        R1*28 |
    }
    {%G
        \mark\default
        R1*10 |
        f1:32\ppp~ |
        1:~ |
        4 r r2 |
        R1*7 |
        e'1:\f |
        1: |
        R1*4 |
        1: |
        1: |
        b4\pp 8 8 8 r16 16 8 8 |
        8 8 8 8 8 8 8 8 |
        4 8-. 8-. 8-.\< r16 16-. 8-. 8-. |
        8-.\mp\< 8-. 8-. 8-. 8-. 8-. 8-. 8-. |
        e4\fz 8-. 8-. 8-. r16 16-. 8-. 8-. |
        8-.\< 8-. 8-. 8-. 8-. 8-. 8-. 8-. |
        R1*6\! |
        g,8\f 8 r4 8 8 r4 |
        8\< 8 r4 8 8 r4 |
    }
    {% H
    	\mark\default
        1:32\ff |
        1: |
        R1*8 |
        1:32\mp\<~ |
        1:~ |
        4\ff 4 r4 4 |
        r4 4 r8 8 4 |
        r4 r r4 r |
        r8 8 8 r8 r4 4 \bar "|."
    }
}

% Temp

\new Score{
    \Timbale
}
