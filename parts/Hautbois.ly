\version "2.19.82"

\include "header.ly"

Hautbois = \relative c''' {
    \StandardConf
    \clef treble \key e \minor
    \tempo "Allegro con fuoco" 4=152

    {%
        R1*7 |
        r4 bes-.\f bes-. bes-. |
        a1\< |
    }%
    {% A
	\mark\default
        g4\f r r2 |
        fis4 r r2 |
        g4 r g r |
        g r r2 |
        g4 r r2 |
        fis4 r r2 |
        e4 r a r |
        g2 e8 r r4 |
        b'4 r r2 |
        c4 r r2 |
        b4 r b r |
        b r r2 |
        b4 r r2 |
        c4 r r2 |
        b4 r a r |
        g2 ~ 8 r r4 |
    }% End A
    {% B
        \mark\default
        R1*7 |
        r2 r4 b,\ff |
        e2-> fis4-. g-. |
        fis4.-> e8 2 |
        e d4 b8 d |
        e2.-> e4 |
        e2-> fis4-. g-. |
        a4. g16 a b2-> |
        g16 fis e4. fis4-. dis-. |
        g16 fis e4. fis4-. dis-. |
        g16( fis) e8 fis-. dis-. g16( fis) e8 fis-. dis-. |
        g16( fis) e8 fis-. dis-. fis-. dis-. fis-. dis-. |
    }% End B
    {% C
        \mark\default
        g8\f  r g r g r g r |
        g r a r b r fis8. e16 |
        fis8 r a r fis r r4 |
        g8 r g r g r g r |
        g r a r b r g8. fis16 |
        g8 r fis r g r r4 |

        \tuplet 3/2 {b,8\( c d} \tuplet 3/2 {c g c} \tuplet 3/2 {b c b} \tuplet 3/2 {a g a} |
        \tuplet 3/2 {g fis g} \tuplet 3/2 {fis e fis} \tuplet 3/2 {e fis e} \tuplet 3/2 {dis e b' \)}
        \tuplet 3/2 {b8\( c d} \tuplet 3/2 {c g c} \tuplet 3/2 {b c b} \tuplet 3/2 {a g a} |
        \tuplet 3/2 {g fis g} \tuplet 3/2 {fis e fis} \tuplet 3/2 {e fis e} \tuplet 3/2 {dis e b' \)}

        e, r g' r g r g r |
        g r a r b r fis8. e16 |
        fis8 r fis r fis r r4 |
        g8 r b r g r d r |
        g4 r r2 |
        fis4 r r2 |
        \tuplet 3/2 {bes8 8 8} r4 \tuplet 3/2 {g8 8 8} r4 |
        \tuplet 3/2 {e8 8 8} r4 \tuplet 3/2 {e8 dis e} r4 |
        R1*2 |
        bes1\p\>~ |
        1\pp~
    }
    {% D
        \mark\default
        \tempo 4=120
        8 r r4 r2 |
        R1*17 |
        a1\p |
        g2.\< b4 |
        c1\! |
        b |
        b |
        c2 d4 c |
        b1 |
        b2\<( a) |
    }
    {% E
        \mark\default
        b1\ff |
        a |
        b |
        a2( b) |
        b1 |
        c |
        b |
        a2 b4. r8 |
        b'4.( a8) b4. 8 |
        a4( c) b( a) |
        b4.( a8) b4. 8 |
        a4( c) b( a) |
        b2 2 |
        fis\prall\trill g8 r r4 |
        R1*4 |
        <b, b'>8\ff r <a a'> r <g g'>2 |
        <b b'>8 r <a a'> r <g g'>2 |
        <b b'>8 r <a a'> r <g g'>2 |
        <b b'>8\> r <a a'> r <g g'>2 |
        b1\mp~ |
        1~ |
        1\p~ |
        1~ |
        4\pp r r2 |
        R1*9 |
    }
   {% F
        \mark\default
        r2 b'8\pp r a r |
        g2\prall\trill fis8 r e r |
        d2\prall\trill c4( b) |
        a\< b c8( b) e d |
        c1~\f |
        1 |
        b~\p |
        2( a) |
        b8 r r4 b'8\pp r a r |
        g2\prall\trill fis8 r e r |
        d2\prall\trill c4( b) |
        a\< b c8( b) e d |
        c1~\f |
        1 |
        b~\p |
        1~ |
        8 r r4 r2 |
        \tuplet 3/2 {g8\fz\> 8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8\!} |
        R1 |
        \tuplet 3/2 {8\fz\> 8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8\!} |
        R1 |
        \tuplet 3/2 {8\fz\> 8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8\!} |
        R1 |
        \tuplet 3/2 {8\p\<8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8} \tuplet 3/2 {8 8 8} |
        bes\f r r4 r2 |
        R1*3 |
   }
   {% G
        \mark\default
        f'4.\p\< a8 2 |
        f4.\! c8 2\> |
        cis f4 4 |
        cis4.\! 8 2 |
        fis,1\pp~ |
        2 r |
        fis'4.\pp ais8 2 |
        fis4. cis8 2 |
        d fis4 4 |
        d4. 8 2 |
        g1~ |
        4. r8 r2 |
        4.\p( b8) 2 |
        g4.( d8) 2 |
        g8 r16 b16 4-. g8 r16 d b4-. |
        8 r16 d16 4-. b8 r16 a g4-. |
        g'8.\p b16 4-> g8. b16 4-> |
        g8.\<-\markup{\italic "cresc. molto"} b16 4-> g8. b16 4-> |
        g8. b16 4-> g8. b16 4-> |
        g8. b16 4-> g8. b16 4-> |
        bes1\ff~ |
        1 |
        ees,8.\p g16 4-> ees8. g16 4-> |
        ees8.\<-\markup{\italic "cresc. molto"} g16 4-> ees8. g16 4-> |
        ees8. g16 4-> ees8. g16 4-> |
        ees8. g16 4-> ees8. g16 4-> |
        bes1\ff~ |
        1 |
        dis,4\p 8-. 8-. 8-. 8-. 8-. 8-. |
        8-. 8-. 8-. 8-. 8-. 8-. 8-. 8-. |
        e4\p 8-. 8-. 8-.\< 8-. 8-. 8-. |
        8-.\mp\< 8-. 8-. 8-. 8-. 8-. 8-. 8-. |
        fis4\fz g8-. a-. g-. r16 fis16 8-. a-. |
        g-.\< fis-. a-. g-. fis-. a-. g-. fis-. |
        a1\ff |
        R1 |
        1 |
        R1 |
        r2 fis4 r4 |
        r2 fis4 r4 |
        bes8\f 8 r4 8 8 r4 |
        8\< 8 r4 8 8 r4 |
   }
   {% H
        \mark\default
        1\ff~ |
        1 |
        4. 16 16 4. 16 16 |
        1 |
        4. 16 16 4. 16 16 |
        1 |
        1\sff |
        1\sff |
        1\sff |
        1\fz |
        g\fff~ |
        1 |
        4-. 4-. r4 4-. |
        r4 4-. r8 8-. 4-. |
        r4 4-. r4 4-. |
        r8 8-. 8-. r8 r4 4\fermata \bar "|."
   }
}

% Temp

\new Score{
    \Hautbois
}
