\version "2.19.82"

\include "header.ly"

Trombone = \relative c {
    \StandardConf
    \tempo "Allegro con fuoco" 4=152
    \clef bass \key e \minor

    {%
        R1*7 |
        r4 ees\f des des |
        c8 b c b c b c b |
    }%
    { % A
        \mark\default
        b4 r r2 |
        c4 r r2 |
        b4 r b r |
        b r r2 |
        b4 r r2 |
        c4 r r2 |
        b4 r a r |
        b2~8 r r4 |

        b4 r r2 |
        c4 r r2 |
        b4 r g r |
        g r r2 |
        b4 r r2 |
        c4 r r2 |
        e4 r dis r |
        e2~8 r r4 |
    }
    {% B
        \mark\default
        R1*8 |
        b4\ff r r2 |
        c4 r r2 |
        b4 r b r |
        b r r2 |
        b4 r r2 |
        e2-> b-> |
        c2 4-. b-. |
        c2 4-. b-. |
        c4 8-. b-. c4 8-. b-. |
        c4 8-. b-. c-. b-. c-. b-. |
    }
    {% C
        \mark\default
        r g\mf r d r g r d |
        r g r a r b  r cis |
        r fis, r fis fis r r4 |
        r8 g\mf r d r g r d |
        r g r a r b  r c |
        r g r fis-. e-. r r4 |
        R1*4 |
        r8 e r b r e r b |
        r e r fis r g r a |
        r d, r d d r r4 |
        r8 e r b r e r b |
        r e r4 r2 |
        r8 e r4 r2 |
        cis'8\f r cis2-> bes4->~ |
        4 g2.->\> |
        R1*4\! |
    }
    {% D
        \mark\default
        \tempo 4=120
        R1*26
    }
    {% E
        \mark\default
        g'4\ff r r2 |    
        g4 r r2 |
        g8 r g r g r g r |
        g r g r g4 r |
        g4 r r2 |
        g4 r r2 |
        8 r g r g r g r |
        g r g r g4 r |
        d'\f r r d |
        r2 ees4 r |
        r d r2 |
        ees4 r r ees |
        d8 r dis r e r e r |
        d r d r d4 r |
        r2 e8.\f( b16) e8 r |
        r2 e8.( b16) e8 r |
        r2 e8.( b16) e8 r |
        r2 e8.( b16) e8 r |
        b,-^ cis-^ dis-^ b-^ e-^ b-^ g-^ e-^ |
        b'-^ cis-^ dis-^ b-^ e-^ b-^ g-^ e-^ |
        d'1\p\> |
        1\> |
        b\mp |
        1 |
        1\p |
        1 |
        4\pp r r2 |
        R1*7 |
        r4\pp\< e-. d-. b-. |
        g-. e-. d-. b-. |
    }
    {% F
        \mark\default
        g1\mp\>
        R1*11\! |
        dis''2->\mf e4-. fis-. |
        e4. dis8 2 |
        b c4 d |
        c b4 4.-> 8 |
        e1\fz |
        R1*11 |
    }
    {% G
        \mark\default
        R1*20 |
        g4.\ff bes8 2 |
        g4. f8 ees2 |
        R1*4 |
        fis4.\ff bes8 2 |
        fis4. f8 ees2 |
        R1*6 |
        c'1->\ff |
        R1 |
        1-> |
        R1 |
        cis2.->\sff r8 8 |
        d2.->\sff r8 8 |
        d4\fz( cis8) r8 d4\fz( cis8) r8 |
        d4\fz( cis8) r8 d4\fz( cis8) r8 |
    }
    {% H
        \mark\default
        d4 r r2 |
        R1*9 |
        e,2->\ff fis4-. g-. |
        fis4.-> e8 2 |
        2-> d4-> b8-. d-. |
        e2. r4 |
        2-> d4-> b8-. d-. |
        e1->\fermata \bar "|."
    }
}

% Temp

\new Score{
    \Trombone
}