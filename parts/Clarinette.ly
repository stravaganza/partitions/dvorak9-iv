\version "2.19.82"

\include "header.ly"

Clarinette = \relative c'' {
    \StandardConf
    \transposition bes
    \tempo "Allegro con fuoco" 4=152
    \clef treble \key fis \minor

    {%
        R1*7 |
        r4 c-.\f c-. c-. |
        b1\< |
    }%
    {% A
        \mark\default
        a4\f r r2 |
        d4 r r2 |
        cis4 r cis r |
        cis r r2 |
        cis4 r r2 |
        d4 r r2 |
        cis4 r cis r |
        cis2 ~ 8 r r4 |
        a r r2 |
        d4 r r2 |
        cis4 r cis r |
        cis r r2 |
        cis4 r r2 |
        d4 r r2 |
        cis4 r cis r |
        cis2 ~ 8 r r4 |
    }% End A 
    {% B
        \mark\default
        R1*7 |
        r2 r4 cis\ff |
        fis2-> gis4-. a-. |
        gis4.-> fis8 2 |
        fis e4 cis8 e |
        fis2.-> 4 |
        fis2-> gis4-. a-. |
        b4. a16 b cis2-> |
        a16 gis fis4. gis4-. eis-. |
        a16 gis fis4. gis4-. eis-. |
        a16( gis) fis8 gis8-. eis-. a16( gis) fis8 gis8-. eis-. |
        a16( gis) fis8 gis8-. eis-. gis8-. eis-. gis8-. eis-. |
    }% End B
    {% C
        \mark\default
        cis8\f r e r cis r e r |
        cis r e r e r dis r |
        e r gis r e r r4 |
        cis8 r e r cis r e r |
        cis r e r e r d r |
        cis r cis r cis r r4 |

        \tuplet 3/2 {cis'8\( d e} \tuplet 3/2 {d a d} \tuplet 3/2 {cis d cis} \tuplet 3/2 {b a b} |
        \tuplet 3/2 {a gis a} \tuplet 3/2 {gis fis gis} \tuplet 3/2 {fis gis fis} \tuplet 3/2 {eis fis cis'\)} |
        \tuplet 3/2 {cis8\( d e} \tuplet 3/2 {d a d} \tuplet 3/2 {cis d cis} \tuplet 3/2 {b a b} |
        \tuplet 3/2 {a gis a} \tuplet 3/2 {gis fis gis} \tuplet 3/2 {fis gis fis} \tuplet 3/2 {eis fis cis'\)} |

        fis, r e r cis r e r |
        cis r e r e r dis r |
        e r e r e r r4 |
        cis8 r e r a r e r |
        cis4 r r2 |
        cis4 r r2 |
        \tuplet 3/2 {fis8 eis fis} r4 \tuplet 3/2 {dis8 cisis dis} r4 |
        \tuplet 3/2 {c8 b c} r4 \tuplet 3/2 {dis8 8 8} r4 |
        r4 \tuplet 3/2 {c8\mf b c} r4 \tuplet 3/2 {c8 b  c} |
        r4 \tuplet 3/2 {fis,8\p 8 8\>} r4 \tuplet 3/2 {fis8 8 8\!} |
        R1*2 |
    }
    {% D
        \mark\default
        \tempo 4=120
        fis'2.\p\< fis4~ |
        4\! 4-- 4-- 4-- |
        fis2.\f( e4\<) |
        b'\! gis e\> d |
        d\! cis e2\p~ |
        2. 4( |
        fis2.\<) e4\mf( |
        b') gis e d |
        d cis e2~ |
        2. 4 |
        g2. fis4 |
        g d c4. a8 |
        b1~ |
        2. d4 |
        g2.( fis4) |
        g\> d c4. a8 |
        b1\p~ |
        2 4. 8 |
        d2.\mf cis4 |
        2.\< fis4 |
        2.\! e4 |
        1~ |
        2 eis|
        fis g4 fis |
        a2( cis,) |
        e1\prall\trill\< |
    }
    {% E
        \mark\default
        1\ff |
        eis |
        fis4( eis) fis2 |
        e1 |
        e |
        eis-> |
        fis4( eis) fis2 |
        e2~ 4. r8 |
        a4.( gis8) a4.( cis,8) |
        b4( d) cis( b) |
        a'4.( gis8) a4.( cis,8) |
        b4( d) cis( b) |
        e( eis fis2) |
        e\prall\trill a8 r r4 |
        r2 fis8.\f( cis16) fis8 r |
        r2 fis8.( cis16) fis8 r |
        r2 fis8.( cis16) fis8 r |
        r2 fis8.( cis16) fis8 r |
        cis'\f r b r a2 |
        cis8 r b r a2 |
        cis8 r b r a2 |
        cis8\> r b r a2 |
        cis,,8-.\mp 8-. b-. b-. a-. a-. a-. b-. |
        cis-. cis b-. b-. a8.-> 16-. 8-. b-. |
        cis8-.\p 8-. b-. b-. a-. a-. a-. b-. |
        cis-. cis b-. b-. a8.-> 16-. 8-. b-. |
        a-.\pp b-. a8.-. 16-. 8-. b-. a-. b-.|
        a8.-> 16-. 8-. b-. a-. b-. a8.-. 16-. |
        8( b) cis4 8 d e cis |
        a4\pp r a r |
        r2 cis'4-.\p b-. |
        a-. r fis-. e-. |
        cis-. r r2 |
        R1*3 |
    }
    {% F
        \mark\default
        r2 a''8\pp r gis r |
        fis2\prall\trill e8 r d r |
        cis2\prall\trill b4( a) |
        gis\< a b8( a) d cis |
        b1~\f |
        1 |
        gis~\p |
        1 |
        a8 r r4 a'8\pp r gis r |
        fis2\prall\trill e8 r d r |
        cis2\prall\trill b4( a) |
        gis\< a b8( a) d cis |
        b1~\f |
        1 |
        1~\p |
        1 |
        a8 r r4 r2 |
        8 r a r a r a r |
        R1 |
        a8 r a r a r a r |
        R1*8 |
    }
    {% G
        \mark\default
        d4.\p\< g8 2 |
        d4.\! b8\> 2 |
        2 dis4 4 |
        b4.\! 8 2 |
        dis1\pp~ |
        2 r |
        4.\pp gis8 2 |
        dis4. bis8 2 |
        2 e4 4 |
        bis4. 8 2 |
        e1~ |
        4. r8 r2 |
        cis4.\p( e8) 2 |
        cis4.( a8) 2 |
        R1 |
        a8 r16 cis16 4-. a8 r16 e cis4-. |
        cis'8.\p e16 4-> cis8. e16 4-> |
        cis8.\<-\markup{\italic "cresc. molto"} e16 4-> cis8. e16 4-> |
        cis8. e16 4-> cis8. e16 4-> |
        cis8. e16 4-> cis8. e16 4-> |
        f1\ff~ |
        1 |
        a,8.\p c16 4-> a8. c16 4-> |
        a8.\<-\markup{\italic "cresc. molto"} c16 4-> a8. c16 4-> |
        a8. c16 4-> a8. c16 4-> |
        a8. c16 4-> a8. c16 4-> |
        f1\ff~ |
        1 |
        cis4\p dis8-. eis-. dis-. r16 cis16 8-. eis-. |
        dis-. cis-. eis-. dis-. cis-. eis-. dis-. cis-. |
        fis4\fp gis8-. a-. gis-.\< r16 fis16 8-. a-. |
        gis-.\mp\< fis-. a-. gis-. fis-. a-. gis-. fis-. |
        d4\fz 8-. 8-. 8-. r16 16 8-. 8-. |
        8-.\< 8-. 8-. 8-. 8-. 8-. 8-. 8-. |
        gis1\ff |
        R1 |
        1 |
        R1 |
        r2 fis4 r |
        r2 cis4 r |
        a'8\f 8 r4 8 8 r4 |
        8\< 8 r4 8 8 r4 |
    }
    {% H
        \mark\default
        e1\ff~ |
        1 |
        4. 16 16 4. 16 16 |
        1\sff |
        4. 16 16 4. 16 16 |
        1\sff |
        1\sff |
        1\sff |
        1\sff |
        fis\fz |
        a~ |
        1 |
        4-. 4-. r4 4-. |
        r4 4-. r8 8-. 4-. |
        r4 4-. r4 4-. |
        r8 8-. 8-. r8 r4 4\fermata \bar "|."
    }
}

% Temp

\new Score{
    \Clarinette
}