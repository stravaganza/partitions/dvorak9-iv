\version "2.19.82"

\include "header.ly"

Trompette = \relative c'' {
    \StandardConf
    \transposition bes
    \tempo "Allegro con fuoco" 4=152
    \clef treble \key fis \minor

    {%
        R1*7 |
        r4 c-.\mf c-. c-. |
        r8\< cis r cis r cis r cis |
    }%
    { % A
        \mark\default
        fis,2->\f gis4-. a-. |
        gis4.-> fis8 2 |
        fis2 e4 cis8 e |
        fis2.-> 4 |
        fis2-> gis4-. a-. |
        gis4.-> fis8 2 |
        fis4 \tuplet 3/2 {a8 fis a} cis4 cis, |
        fis2~8 r r4 |

        <fis fis'>2->\ff <gis gis'>4-. <a a'>-. |
        <gis gis'>4.-> <fis fis'>8 2 |
        <fis fis'>2 <e e'>4 <cis cis'>8 <e e'> |
        <fis fis'>2.-> 4 |
        <fis fis'>2-> <gis gis'>4-. <a a'>-. |
        <gis gis'>4.-> <fis fis'>8 2 |
        <fis fis'>4 \tuplet 3/2 {<a a'>8 <fis fis'> <a a'>} <cis cis'>4 <cis, cis'> |
        <fis fis'>2~8 r r4 |
    }
    {% B
        \mark\default
        R1*8 |
        cis'4\f r r2 |
        <fis, fis'>4 r r2 |
        cis'4 r cis r |
        cis r r2 |
        cis4 r r2 |
        b4-> a8-. b-. cis2-> |
        d d4-. cis-. |
        d2 4-. cis-. |
        d4 8-. cis-. d4 8-. cis-. |
        d4 8-. cis-. d-. cis-. d-. cis-. |
    }
    {% C
        \mark\default
        fis8\f r r4 fis8 r r4 |
        fis8 r r4 r2 |
        R1 |
        fis8 r r4 fis8 r r4 |
        fis8 r r4 4 fis8 r |
        cis r cis r fis r r4 |
        R1*4 |
        fis8 r r4 fis8 r r4 |
        fis8 r r4 r2 |
        R1 |
        fis8 r a, r fis' r a, r |
        fis' r r4 r2 |
        cis8 r r4 r2 |
        r4 <fis, fis'> r <fis fis'> |
        r <fis fis'> r <fis fis'> |
        R1*4 |
    }
    {% D
        \mark\default
        \tempo 4=120
        R1*26
    }
    {% E
        \mark\default
        cis'2\f r8 16 16 8 8 |
        gis2 r4 4 |
        cis-. 4-. r8 16 16 8 8 |
        gis4 8. 16 cis4 r |
        2 r8 16 16 8 8 |
        gis2 r4 4 |
        cis-. 4-. r8 16 16 8 8 |
        gis4 8. 16 cis4 r |
        cis2\mf r8 16 16 8 8 |
        gis2 r4 \tuplet 3/2 {gis8 8 8} |
        cis4-. 4-. r8 16 16 8 8 |
        gis4 \tuplet 3/2 {8 8 8} 4 4 |
        cis4. 16 16 8 16 16 8 8 |
        gis r \tuplet 3/2 {8 8 8} cis4 r |
        R1*22 |
    }
    {% F
        \mark\default
        R1*7 |
        e4.\pp 16-. 16-. 4-. 4-. |
        4 r r2 |
        R1*19 |
    }
    {% G
        \mark\default
        R1*20 |
        a,4.\ff c8 2 |
        a4. g8 f2 |
        R1*4 |
        gis4.\ff c8 2 |
        gis4. g8 eis2 |
        cis'4\pp 8 8 8 r16 16 8 8 |
        8 8 8 8 8 8 8 8 |
        4 8-. 8-. 8-.\< r16 16-. 8-. 8-. |
        8-.\mp\< 8-. 8-. 8-. 8-. 8-. 8-. 8-. |
        fis4\fz 8-. 8-. 8-. r16 16-. 8-. 8-. |
        8-.\< 8-. 8-. 8-. 8-. 8-. 8-. 8-. |
        <gis, gis'>1\ff |
        R1 |
        1-> |
        R1 |
        r2 4 r |
        r2 4 r |
        R1*2 |
    }
    {% H
        \mark\default
        a2->\ff b4-. c-. |
        b4.-> a8 2 |
        2-> g4 e8-. g-. |
        a2.-> r4 |
        2-> g4 e8-. g-. |
        a2.-> r4 |
        a2.->\fz r4 |
        c2.->\fz r4 |
        e2.->\fz r4 |
        fis1->\ff |
        c->~ |
        1 |
        4-. 4-. r4 4-. |
        r4 4-. r8 8-. 4-. |
        r4 4-. r4 4-. |
        r8 8-. 8-. r8 r4 4\fermata \bar "|."
    }
}

% Temp

\new Score{
    \Trompette
}