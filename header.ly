\header {
    title = "Symphony No.9"
	subtitle = "\"From the New World\""
	subsubtitle = "IV"
    composer = "Antonín Dvořák"
	arrangement = "Stravaganzé par Rémy H."
	tagline = "Stravaganza"
	date = "P19"
  }

StandardConf =
{
	\set Score.markFormatter = #format-mark-box-alphabet
	\compressFullBarRests
	\override MultiMeasureRest.expand-limit = #1
	\override BreathingSign.Y-offset = #3
}

