\version "2.19.82"

%include separate files

%\include"instrument.ly"
\include "parts/Violons.ly"
\include "parts/Violoncelle.ly"
\include "parts/Flute.ly"
\include "parts/Hautbois.ly"
\include "parts/Clarinette.ly"
\include "parts/SaxAlto.ly"
\include "parts/Cor.ly"
\include "parts/Trompette.ly"
\include "parts/SaxTenor.ly"
\include "parts/Trombone.ly"
\include "parts/Timbale.ly"
\include "parts/Piano.ly"
\include "parts/Harpe.ly"
\include "parts/Alto.ly"
\include "parts/Violon2.ly"
\include "parts/Tuba.ly"

#(define-markup-command (vspace layout props amount) (number?)
  "This produces a invisible object taking vertical space."
  (let ((amount (* amount 3.0)))
    (if (> amount 0)
        (ly:make-stencil "" (cons -1 1) (cons 0 amount))
        (ly:make-stencil "" (cons -1 1) (cons amount amount)))))

#(define-markup-command (when-property layout props symbol markp) (symbol? markup?)
  (if (chain-assoc-get symbol props)
      (interpret-markup layout props markp)
      (ly:make-stencil '()  '(1 . -1) '(1 . -1))))
#(set-global-staff-size 10)
%partie globale
\book {
  \bookOutputName "PDF/New_World_Syphony_conducteur"
	\paper {
    line-width	= 180\mm
    left-margin   = 20\mm
    top-margin	= 10\mm
    bottom-margin = 20\mm
    max-systems-per-page = 1
    ragged-last-bottom = ##f
    %ragged-bottom=##t
    markup-system-spacing = #'((basic-distance . 5) (padding . 5) (stretchability . 10))

  	bookTitleMarkup = \markup \when-property #'header:title \column {
	  \combine \null \vspace #28
	  \fill-line { \postscript #"-20 0 moveto 40 0 rlineto stroke" }
	  \combine \null \vspace #6
	  \fill-line { \fontsize #14 \fromproperty #'header:title }
	  \combine \null \vspace #6
	  \fill-line { \fontsize #10 \fromproperty #'header:subtitle }
	  \combine \null \vspace #6
	  \fill-line { \fontsize #9 \fromproperty #'header:subsubtitle }
	  \combine \null \vspace #6
	  \fill-line { \fontsize #8 \italic \fromproperty #'header:composer }
	  \combine \null \vspace #4
	  \fill-line { \fontsize #4444 \italic \fromproperty #'header:compDates }
	  \combine \null \vspace #4
	  \fill-line { \fontsize #8 \italic \fromproperty #'header:tagline }
	  \combine \null \vspace #6
	  \fill-line { \postscript #"-20 0 moveto 40 0 rlineto stroke" }
	  \combine \null \vspace #6
	  \fill-line { \fontsize #5 \fromproperty #'header:date }
	  \combine \null \vspace #1
	  \fill-line {
		\when-property #'header:arrangement \column {
		  \combine \null \vspace #5
		  \fill-line { \fontsize #5 \fromproperty #'header:arrangement }
		}
	  }

	}
  scoreTitleMarkup = \markup \null
}

  \header {
    title = "Symphony No.9"
		subtitle = "\"From the New World\""
		subsubtitle = "IV"
    composer = "Antonín Dvořák"
		arrangement = "Stravaganzé par Rémy H."
		tagline = "Stravaganza"
		date = "P19"
  }


   %définition de la partition
\score {
  \header
  {
  breakbefore = ##t
  }
<<

  \context StaffGroup = Bois <<
    \context Staff = Flutes <<
          \set Staff.instrumentName = "Flutes"
          \set Staff.shortInstrumentName = "Flts."
          \context Voice = Flutes \Flute
    >>
    \context Staff = Hautbois <<
          \set Staff.instrumentName = "Hautbois"
          \set Staff.shortInstrumentName = "Htbs."
          \context Voice = Hautbois \Hautbois
    >>
    \context Staff = Clarinettes <<
          \set Staff.instrumentName = "Clarinettes"
          \set Staff.shortInstrumentName = "Clar."
          \context Voice = Clarinette \Clarinette
    >>
    \context Staff = SaxTenor <<
          \set Staff.instrumentName = "Saxophone Ténor"
          \set Staff.shortInstrumentName = "SaxT."
          \context Voice = SaxTenor \SaxTenor
    >>
    \context Staff = SaxAlto <<
          \set Staff.instrumentName = "Saxophone Alto"
          \set Staff.shortInstrumentName = "SaxA."
          \context Voice = SaxAlto \SaxAlto
    >>
  >>%end of bois

  \context StaffGroup = Cuivres <<
    \context Staff = Trompettes <<
          \set Staff.instrumentName = "Trompettes"
          \set Staff.shortInstrumentName = "Trmp."
          \context Voice = Trompette \Trompette
    >>
    \context Staff = Trombone <<
          \set Staff.instrumentName = "Trombone"
          \set Staff.shortInstrumentName = "Trmb."
          \context Voice = Trombone \Trombone
    >>
    \context Staff = Cor <<
          \set Staff.instrumentName = "Cor en Fa"
          \set Staff.shortInstrumentName = "Cor"
          \context Voice = Cor \Cor
    >>
    \context Staff = Tuba <<
          \set Staff.instrumentName = "Tuba contrebasse"
	  \set Staff.shortInstrumentName = "Tuba"
	  \context Voice = Tuba \Tuba
    >>
  >> % end of cuivres

  \context StaffGroup = Percus <<
    \context Staff = Timbale <<
          \set Staff.instrumentName = "Timbale"
          \set Staff.shortInstrumentName = "Timb."
          \context Voice = Timbale \Timbale
    >>
  >> % end of percus

  \context StaffGroup = PianoHarpe <<
    \context PianoStaff = Piano <<
          \set PianoStaff.instrumentName = "Piano"
          \set PianoStaff.shortInstrumentName = "Piano"
          \context Voice = PianoD \PianoD
          \context Voice = PianoL \PianoL
    >>
    \context PianoStaff = Harpe <<
          \set PianoStaff.instrumentName = "Harpe"
          \set PianoStaff.shortInstrumentName = "Harpe"
          \context Voice = HarpeD \HarpeD
          \context Voice = HarpeL \HarpeL
    >>
  >> %end of PianoHarpe

  \context StaffGroup = Cordes <<
    \context Staff = ViolonsI <<
          \set Staff.instrumentName = "Violons I"
          \set Staff.shortInstrumentName = "Vlns.I"
          \context Voice = ViolonsI \ViolonI
        >>

    \context Staff = ViolonsII <<
          \set Staff.instrumentName = "Violons II"
          \set Staff.shortInstrumentName = "Vlns.II"
          \context Voice = ViolonsII \ViolonII
        >>

    \context Staff = Altos <<
          \set Staff.instrumentName = "Altos"
          \set Staff.shortInstrumentName = "Alt."
          \context Voice = Altos \Alto
        >>

    \context Staff = Violoncelles <<
          \set Staff.instrumentName = "Violoncelles"
          \set Staff.shortInstrumentName = "Vlcs."
          \context Voice = Violoncelles \Violoncelle
        >>
  >> %end of corder

  \set Score.markFormatter = #format-mark-alphabet
  \set Score.skipBars = ##t
  %%\set Score.melismaBusyProperties = #'()
  \override Score.BarNumber #'break-visibility = #end-of-line-invisible %%every bar is numbered.!!!
  %% remove previous line to get barnumbers only at beginning of system.

  >>

  %% Boosey and Hawkes, and Peters, have barlines spanning all staff-groups in a score,
  %% Eulenburg and Philharmonia, like Lilypond, have no barlines between staffgroups.
  %% If you want the Eulenburg/Lilypond style, comment out the following line:
  \layout {
  %#(layout-set-staff-size 14)
  %\context {\Score \consists Span_bar_engraver}
  }
  %\midi{}
  }
}
